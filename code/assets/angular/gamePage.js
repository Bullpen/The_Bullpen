mainApp.controller('gamePage', function($scope, $http) {
    var gameId = window.location.pathname.split('/')[2];

    $http.get("/api/game/" + gameId).then(function(data) {
        data = data.data;

        $scope.homeTeam = data.homeTeam;
        $scope.awayTeam = data.awayTeam;
        $scope.homeScore = data.homeScore;
        $scope.awayScore = data.awayScore;
        $scope.gameTime = formatDate(data.gameTime);
        $scope.numInnings = data.numInnings;
        $scope.homeHits = data.homeHits;
        $scope.awayHits = data.awayHits;
        $scope.winningPitcher = findPitcherName(data.winningPitcher, data, true);
        $scope.winPitchID = data.winningPitcher;
        $scope.losePitchID = data.losingPitcher;
        $scope.losingPitcher = findPitcherName(data.losingPitcher, data, false);

        if (data.savePitcher) {
            $scope.savePitcher = findPitcherName(data.savePitcher, data, true);
            $scope.svPitchID = data.savePitcher;
        }


        $scope.homePlayers = data.homePlayers;
        $scope.awayPlayers = data.awayPlayers;

    });
});

function formatDate(gameTime) {
    var date = new Date(gameTime);
    return date.getMonth() + '/' + date.getDate() + "/" + date.getFullYear();
}


function findPitcherName(pId, data, teamIsWinner) {

    var players;

    for (var player_i in (players = ((data.winner == data.homeTeam) == teamIsWinner) ?
            data.homePlayers.defensive : data.awayPlayers.defensive)) {
        if (players[player_i].playerId == pId) return players[player_i].name;
    }

    return "Pitcher Not Found";

}