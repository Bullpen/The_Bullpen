mainApp.controller('account', function($scope, $http) {

    $scope.name = "";
    $scope.accountType = "";
    $scope.request;
    $scope.pendingRequests = [];

    $scope.teams = [];

    $scope.upgradeRequest = {};

    $scope.addPlayer = {};

    $http.get('/api/account').then(function(resultInfo) {
        var result = resultInfo.data;
        console.log(result);
        if (result.hasAccount) {
            $scope.name = result.name;
            $scope.accountType = result.accountType;
            $scope.team = result.team;
            $scope.request = result.request;

            //set up dates
            $scope.request.time = new Date($scope.request.time);
            $scope.request.processTime = new Date($scope.request.processTime);
            if (result.accountType == 'admin') {
                getRequests();
            } else if (result.accountType == 'basic' && !result.request.processor) {
                getValidTeams();
            }
        } else {
            window.location = window.location.origin + '/signIn';
        }
    });


    var getRequests = function() {
        $http.get('/api/requests').then(function(result) {
            $scope.pendingRequests = result.data;

            for (var i = 0; i < $scope.pendingRequests.length; i++) {
                $scope.pendingRequests[i].requestTime = new Date($scope.pendingRequests[i].requestTime);
            }
        });
    };

    var getValidTeams = function() {
        $http.get('/api/noOnesTeams').then(function(result) {
            $scope.teams = result.data;
        });
    };

    $scope.submitUpgradeFrom = function() {
        $http.post('/api/request/make', {
            "upgradeTo": $scope.upgradeRequest.type,
            "team": $scope.upgradeRequest.team
        }).
        then(function(result) {
            window.location.reload();
        });
    };

    $scope.respondToRequest = function(requestId, status) {
        console.log(requestId);
        console.log(status);
        $http.post('/api/request/respond', {
            "requestId": requestId,
            "status": status
        }).
        then(function(result) {
            window.location.reload();
        });
    };

    $scope.addPlayer = function() {
        var request = {
            firstName: $scope.addPlayer.firstName,
            lastName: $scope.addPlayer.lastName,
            team: $scope.team,
            num: $scope.addPlayer.number
        };
        $http.post('/api/player', request).
        then(function(result) {
            window.location = window.location.origin + '/player/' + result.data.playerNum;
        });
    }
});