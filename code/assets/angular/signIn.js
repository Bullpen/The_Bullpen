mainApp.controller('signIn', function($scope, $http) {
    $scope.myForm = {};
    $scope.error = false;

    $scope.submitSignIn = function() {
        console.log($scope.myForm);
        $http.post('/api/signIn', $scope.myForm).
        then(function(result) {
            window.location = window.location.origin + '/account';
        }).
        catch(function(err) {
            console.log(err);
            $scope.error = true;
        });
    }

});