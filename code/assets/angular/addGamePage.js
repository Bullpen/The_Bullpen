mainApp.controller('addGamePage', function($scope, $http, $window) {

    $scope.homeTeam = "";
    $scope.awayTeam = "";
    $scope.homeTeamPlayers = [];
    $scope.awayTeamPlayers = [];
    $scope.accountTypeAdmin = false;
    $scope.loading = false;
    $scope.submitError = false;

    $http.get('/api/teams').then(function(data) {
        $scope.teams = data.data;

        $scope.awayTeams = $scope.teams.slice();
        $scope.homeTeams = $scope.teams.slice();

    });

    $http.get('/api/account').then(function(response) {
        console.log(response);
        if (response.data.accountType == 'manager') {
            $scope.homeTeam = response.data.team;
        } else if (response.data.accountType == 'admin') {
            $scope.accountTypeAdmin = true;
        }
    });


    $scope.teamsSelected = false;

    $scope.updateTeams = function() {

        $scope.awayTeams = $scope.teams.slice();
        $scope.homeTeams = $scope.teams.slice();

        var removeAwayTeam = $scope.homeTeam;
        var indexAway = $scope.awayTeams.indexOf(removeAwayTeam);
        if (indexAway >= 0) {
            $scope.awayTeams.splice(indexAway, 1);
        }
        var removeHomeTeam = $scope.awayTeam;
        var indexHome = $scope.homeTeams.indexOf(removeHomeTeam);
        if (indexHome >= 0) {
            $scope.homeTeams.splice(indexHome, 1);
        }
    };

    $scope.selectTeams = function() {
        $scope.teamsSelected = true;

        $scope.getTeams();

    };

    $scope.homePlayers = {};
    $scope.awayPlayers = {};
    $scope.homePlayers.offensive = [{}];
    $scope.homePlayers.defensive = [{}];
    $scope.awayPlayers.offensive = [{}];
    $scope.awayPlayers.defensive = [{}];



    $scope.getTeams = function() {

        var homeTeam = $scope.homeTeam;
        var awayTeam = $scope.awayTeam;

        $http.get('api/team/' + homeTeam).then(function(response) {
            $scope.homeTeamPlayers = response.data.players;
            $scope.homeTeamPitchers = $scope.homeTeamPlayers.slice();
            $scope.homeTeamBatters = $scope.homeTeamPlayers.slice();


        });

        $http.get('api/team/' + awayTeam).then(function(response) {
            $scope.awayTeamPlayers = response.data.players;
            $scope.awayTeamPitchers = $scope.awayTeamPlayers.slice();
            $scope.awayTeamBatters = $scope.awayTeamPlayers.slice();


        });

    };


    $scope.getPlayer = function(id, home) {
        var players = [];
        if (home) {
            players = $scope.homeTeamPlayers;
        } else {
            players = $scope.awayTeamPlayers;
        }

        for (var player in players) {
            if (id == players[player].playerId) {
                return players[player];
            }
        }

        return null;
    };


    $scope.getPlayerInfo = function(id, home) {
        var player = $scope.getPlayer(id, home);
        if (player) {
            return '#' + player.num + ' ' + player.name;

        }
        return 'Player Not Found';
    };

    $scope.updatePlayerList = function(id, offense, home) {
        var playerList = [];

        if (offense && home) {
            playerList = $scope.homeTeamBatters;
            $scope.homeBatterAdded = true;
        } else if (offense && !home) {
            playerList = $scope.awayTeamBatters;
            $scope.awayBatterAdded = true;
        } else if (!offense && home) {
            playerList = $scope.homeTeamPitchers;
            $scope.homePitcherAdded = true;
        } else if (!offense && !home) {
            playerList = $scope.awayTeamPitchers;
            $scope.awayPitcherAdded = true;
        } else return;

        for (var index = 0; index < playerList.length; index++) {
            if (playerList[index].playerId == id) {
                playerList.splice(index, 1);
                return;
            }
        }

    };

    $scope.addPlayerToGame = function(offense, home) {
        if (offense && home) {
            $scope.homePlayers.offensive.push({});
            $scope.homeBatterAdded = false;
        } else if (offense && !home) {
            $scope.awayPlayers.offensive.push({});
            $scope.awayBatterAdded = false;
        } else if (!offense && home) {
            $scope.homePlayers.defensive.push({});
            $scope.homePitcherAdded = false;
        } else if (!offense && !home) {
            $scope.awayPlayers.defensive.push({});
            $scope.awayPitcherAdded = false;
        }
    };


    $scope.homeTeamPitchers = [];
    $scope.awayTeamPitchers = [];
    $scope.homeTeamBatters = [];
    $scope.awayTeamBatters = [];

    $scope.homePitcherAdded = false;
    $scope.awayPitcherAdded = false;
    $scope.homeBatterAdded = false;
    $scope.awayBatterAdded = false;

    $scope.getTeamPitchersByHome = function(home) {
        var pitchers = [];

        var pitched = home ? $scope.homePlayers.defensive : $scope.awayPlayers.defensive;

        for (var pitcher in pitched) {
            pitchers.push($scope.getPlayer(pitched[pitcher].playerId, home));
        }

        return pitchers;

    };

    $scope.getTeamPitchersByWin = function(win) {
        if (($scope.homeScore > $scope.awayScore) == win) return $scope.getTeamPitchersByHome(true);
        else return $scope.getTeamPitchersByHome(false);
    };

    $scope.submit = function() {

        $scope.loading = true;

        $scope.addTitleToPitcherByHome(true, $scope.homeStarter, 'starter');
        $scope.addTitleToPitcherByHome(false, $scope.awayStarter, 'starter');
        $scope.addTitleToPitcherByWin(true, $scope.winningPitcher, 'winner');
        $scope.addTitleToPitcherByWin(false, $scope.losingPitcher, 'looser'); //sic
        if ($scope.savePitcher) $scope.addTitleToPitcherByWin(true, $scope.savePitcher, 'save');

        var winner = ($scope.homeScore > $scope.awayScore) ? $scope.homeTeam : $scope.awayTeam;

        var addGameData = {
            'homeTeam': $scope.homeTeam,
            'awayTeam': $scope.awayTeam,
            'winner': winner,
            'numInnings': $scope.numInnings,
            'homeScore': $scope.homeScore,
            'awayScore': $scope.awayScore,
            'homeHits': $scope.numHits(true),
            'awayHits': $scope.numHits(false),
            'homePlayers': $scope.homePlayers,
            'awayPlayers': $scope.awayPlayers
        };

        $http.post('/api/game', addGameData).
        then(function(response) {
            var gameId = response.data.gameId;
            $window.location.href = '/game/' + gameId;
        }).
        catch(function(error) {
            $scope.code = error.status;
            $scope.loading = false;
            $scope.submitError = true;
        });

        console.log(addGameData);

    };

    $scope.addTitleToPitcherByHome = function(home, id, title) {
        var pitchers = home ? $scope.homePlayers.defensive : $scope.awayPlayers.defensive;

        for (var pitcher in pitchers) {
            if (pitchers[pitcher].playerId == id) {
                pitchers[pitcher][title] = true;
            }
        }
    };

    $scope.addTitleToPitcherByWin = function(win, id, title) {
        if (($scope.homeScore > $scope.awayScore) == win) $scope.addTitleToPitcherByHome(true, id, title);
        else return $scope.addTitleToPitcherByHome(false, id, title);
    };

    $scope.numHits = function(home) {
        var players = home ? $scope.homePlayers.offensive : $scope.awayPlayers.offensive;
        var numHits = 0;

        for (var player_i in players) {
            var playerStats = players[player_i].stats;
            numHits += playerStats.B1 + playerStats.B2 + playerStats.B3 + playerStats.HR;
        }

        return numHits;

    };

    $scope.resetTeams = function() {

        $scope.numInnings = null;
        $scope.homeScore = null;
        $scope.awayScore = null;

        $scope.awayTeams = $scope.teams.slice();
        $scope.homeTeams = $scope.teams.slice();

        $scope.homePlayers = {
            offensive: [{}],
            defensive: [{}]
        };
        $scope.awayPlayers = {
            offensive: [{}],
            defensive: [{}]
        };
        $scope.homePitcherAdded = false;
        $scope.awayPitcherAdded = false;
        $scope.homeBatterAdded = false;
        $scope.awayBatterAdded = false;
        $scope.teamsSelected = false;
        $scope.awayTeam = "";
        if ($scope.accountTypeAdmin) $scope.homeTeam = "";

    };

    $scope.resetPlayers = function(offense, home) {

        if (offense && home) {
            $scope.homePlayers.offensive = [{}];
            $scope.homeTeamBatters = $scope.homeTeamPlayers.slice();
            $scope.homeBatterAdded = false;
        } else if (offense && !home) {
            $scope.awayPlayers.offensive = [{}];
            $scope.awayTeamBatters = $scope.awayTeamPlayers.slice();
            $scope.awayBatterAdded = false;
        } else if (!offense && home) {
            $scope.homePlayers.defensive = [{}];
            $scope.homeTeamPitchers = $scope.homeTeamPlayers.slice();
            $scope.homePitcherAdded = false;
        } else if (!offense && !home) {
            $scope.awayPlayers.defensive = [{}];
            $scope.awayTeamPitchers = $scope.awayTeamPlayers.slice();
            $scope.awayPitcherAdded = false;
        }
    };


});