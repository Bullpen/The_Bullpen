var mainApp = angular.module("mainApp", []);

mainApp.controller('header', function($scope, $http) {
    $scope.errorMessage = ""; //text to desplay if search returned nothing
    $scope.enableSearch = true; //enable to disable search submit
    $scope.form = {}; //thing to search for
    $scope.search = function() {
        $scope.enableSearch = false;
        $http.get('/search?q=' + $scope.form.query).then(function(result) {
            var resultO = result.data;
            if (resultO.error) {
                $scope.errorMessage = resultO.error.message;
            } else {
                window.location = window.location.origin + resultO.url;
            }
            $scope.enableSearch = true;
        });
    };


    $scope.hasAccount = false;
    $scope.name = "";
    $scope.accountType = "";
    angular.element(document).ready(function() {
        $http.get('/api/account').then(function(result) {
            result = result.data;
            if (result.hasAccount) {
                $scope.hasAccount = true;
                $scope.name = result.name;
                $scope.accountType = result.accountType;
            }
        });
    });

    $scope.logOut = function() {
        $http.get('/api/signOff').then(function() {
            location.reload();
        });
    };
});

mainApp.controller('footer', function($scope) {

});