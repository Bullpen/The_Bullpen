mainApp.controller('teamPage', function($scope, $http) {

    $scope.teamName = window.location.pathname.split('/')[2];
    $scope.teamWins = 162;
    $scope.teamLosses = 0;
    $scope.games = [];
    $scope.players = [];
    $scope.awayMatch = [];

    $http.get('/api/team/' + $scope.teamName).then(function(data) {
        data = data.data;
        $scope.teamWins = data.wins;
        $scope.teamLosses = data.losses;
        $scope.games = data.games;
        $scope.players = data.players;

    });

    $scope.formattedDate = function(dateString) {
        var date = new Date(dateString);
        return date.getMonth() + '/' + date.getDate() + "/" + date.getFullYear();
    };

    $scope.getTeamName = function(game, teamName) {
        return (game.homeTeam != teamName) ? game.homeTeam : game.awayTeam;
    };

    $scope.isAwayTeam = function(game) {
        return (game.homeTeam != $scope.teamName);
    };

    $scope.getFormattedScore = function(game) {
        return (game.homeScore > game.awayScore) ? game.homeScore + ' - ' + game.awayScore :
            game.awayScore + ' - ' + game.homeScore;
    };

    $scope.getOutcome = function(game, teamName) {
        return ((game.homeScore > game.awayScore) == (game.homeTeam == teamName)) ? 'W' : 'L';
    };


});