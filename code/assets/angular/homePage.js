mainApp.controller('homePage', function($scope, $http) {
    $http.get('/api/home').then(function(response) {
        $scope.games = response.data.recentGames;
        $scope.standings = response.data.topTeams;
    });

    $scope.formattedDate = function(dateString) {
        var date = new Date(dateString);
        return date.getMonth() + '/' + date.getDate() + "/" + date.getFullYear();
    };
});