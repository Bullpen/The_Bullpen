mainApp.controller('signUp', function($scope, $http) {
    $scope.myForm = {};
    $scope.error = false;

    $scope.SubmitSignUp = function() {
        console.log($scope.myForm);
        $http.post('/api/signUp', $scope.myForm).
        then(function(result) {
            window.location = window.location.origin + '/account';
        }).
        catch(function(err) {
            console.log(err);
            $scope.error = true;
        });
    }
});