mainApp.controller('playerPage', function($scope, $http) {
    var playerID = window.location.pathname.split('/')[2];

    // set up w/ defaults player info
    $scope.playerName = 'First Last';
    $scope.uniformNumber = 0;
    $scope.team = 'Team Name';
    $scope.recentGames = [];

    // offensive stats
    $scope.games = 0;
    $scope.plateAppearances = 0;
    $scope.avg = '--';
    $scope.obp = '--';
    $scope.singles = 0;
    $scope.doubles = 0;
    $scope.triples = 0;
    $scope.homers = 0;
    $scope.hits = 0;
    $scope.walks = 0;
    $scope.hbp = 0;
    $scope.strikeouts = 0;
    $scope.bbPercent = '--';
    $scope.kPercent = '--';
    $scope.sacFlies = 0;
    $scope.runs = 0;
    $scope.rbi = 0;
    $scope.sb = 0;
    $scope.cs = 0;
    $scope.sbPercent = '--';
    $scope.babip = '--';
    $scope.wOBA = '--';
    $scope.wRCplus = '--';

    // pitching stats
    $scope.pGames = 0;
    $scope.pGamesStarted = 0;
    $scope.pIP = 0;
    $scope.pPartialIP = 0;
    $scope.pWins = 0;
    $scope.pLosses = 0;
    $scope.pSaves = 0;
    $scope.pHits = 0;
    $scope.pHomers = 0;
    $scope.pStrikeouts = 0;
    $scope.pWalks = 0;
    $scope.pHBP = 0;
    $scope.pK9 = '--';
    $scope.pBB9 = '--';
    $scope.pRuns = 0;
    $scope.pEarnedRuns = 0;
    $scope.pERA = '--';
    $scope.pFIP = '--';

    $scope.awayMatch = false;


    $http.get('/api/player/' + playerID).then(function(data) {
        // console.log(data);
        data = data.data;

        // personal stats
        if (data.name != null) $scope.playerName = data.name;
        if (data.num != null) $scope.uniformNumber = data.num;
        if (data.team != null) $scope.team = data.team;

        // offensive stats
        if (data.stats.g != null) $scope.games = data.stats.g;
        if (data.stats.pa != null) $scope.plateAppearances = data.stats.pa;
        if (data.stats.bavg != null) $scope.avg = data.stats.bavg.toFixed(3);
        if (data.stats.obp != null) $scope.obp = data.stats.obp.toFixed(3);
        if (data.stats.b1 != null) $scope.singles = data.stats.b1;
        if (data.stats.b2 != null) $scope.doubles = data.stats.b2;
        if (data.stats.b3 != null) $scope.triples = data.stats.b3;
        if (data.stats.hr != null) $scope.homers = data.stats.hr;
        if (data.stats.h != null) $scope.hits = data.stats.h;
        if (data.stats.bb != null) $scope.walks = data.stats.bb;
        if (data.stats.hbp != null) $scope.hbp = data.stats.hbp;
        if (data.stats.k != null) $scope.strikeouts = data.stats.k;
        if (data.stats.bbp != null) $scope.bbPercent = (data.stats.bbp * 100.0).toFixed(1) + ' %';
        if (data.stats.kp != null) $scope.kPercent = (data.stats.kp * 100.0).toFixed(1) + ' %';
        if (data.stats.sf != null) $scope.sacFlies = data.stats.sf;
        if (data.stats.r != null) $scope.runs = data.stats.r;
        if (data.stats.rbi != null) $scope.rbi = data.stats.rbi;
        if (data.stats.sb != null) $scope.sb = data.stats.sb;
        if (data.stats.cs != null) $scope.cs = data.stats.cs;
        if (data.stats.sbp != null) $scope.sbPercent = (data.stats.sbp * 100.0).toFixed(1) + ' %';
        if (data.stats.babip != null) $scope.babip = data.stats.babip.toFixed(3);
        if (data.stats.woba != null) $scope.wOBA = data.stats.woba.toFixed(3);
        if (data.stats.wrcplus != null) $scope.wRCplus = data.stats.wrcplus;

        // pitching stats
        if (data.stats.gpitcher != null) $scope.pGames = data.stats.gpitcher;
        if (data.stats.gspitcher != null) $scope.pGamesStarted = data.stats.gspitcher;
        if (data.stats.ippitcher != null) $scope.pIP = data.stats.ippitcher;
        if (data.stats.ipfracpitcer != null) $scope.pPartialIP = data.stats.ipfracpitcer;
        if (data.stats.wpitcher != null) $scope.pWins = data.stats.wpitcher;
        if (data.stats.lpitcher != null) $scope.pLosses = data.stats.lpitcher;
        if (data.stats.svpitcher != null) $scope.pSaves = data.stats.svpitcher;
        if (data.stats.hpitcher != null) $scope.pHits = data.stats.hpitcher;
        if (data.stats.hrpitcher != null) $scope.pHomers = data.stats.hrpitcher;
        if (data.stats.kpitcher != null) $scope.pStrikeouts = data.stats.kpitcher;
        if (data.stats.bbpitcher != null) $scope.pWalks = data.stats.bbpitcher;
        if (data.stats.hbppitcher != null) $scope.pHBP = data.stats.hbppitcher;
        if (data.stats.kper9pitcher != null) $scope.pK9 = data.stats.kper9pitcher.toFixed(2);
        if (data.stats.bbper9pitcher != null) $scope.pBB9 = data.stats.bbper9pitcher.toFixed(2);
        if (data.stats.rpitcher != null) $scope.pRuns = data.stats.rpitcher;
        if (data.stats.erpitcher != null) $scope.pEarnedRuns = data.stats.erpitcher;
        if (data.stats.erapitcher != null) $scope.pERA = data.stats.erapitcher.toFixed(2);
        if (data.stats.fippitcher != null) $scope.pFIP = data.stats.fippitcher.toFixed(2);

        // recent games
        if (data.recentGames != null) $scope.recentGames = data.recentGames;

        // add to controller

    });

    $scope.formattedDate = function(dateString) {
        var date = new Date(dateString);
        return date.getMonth() + '/' + date.getDate() + "/" + date.getFullYear();
    };

    $scope.getTeamName = function(game, teamName) {
        return (game.homeTeam != teamName) ? game.homeTeam : game.awayTeam;
    };

    $scope.isAwayGame = function(game) {
        return (game.homeTeam != $scope.team);
    };

    $scope.getFormattedScore = function(game) {
        return (game.homeScore > game.awayScore) ? game.homeScore + ' - ' + game.awayScore :
            game.awayScore + ' - ' + game.homeScore;
    };

    $scope.getOutcome = function(game, team) {
        return (game.winner == team) ? 'W' : 'L'
    };

});