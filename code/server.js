var express = require('express');
var bodyParser = require('body-parser');
var routes = require('./routes/routes.js');
var less = require('./compile.js');
var session = require('client-sessions');

//call back is so the css is compiled and then the server will start
less.compile(serverCreation);

function serverCreation() {
    var app = express();
    app.use(express.static(__dirname + '/assets'));

    app.use(bodyParser.json()); // to support JSON-encoded bodies

    app.use(session({
      cookieName: 'session', //name of the cookie
      secret: 'supper secret key', //this is the encription key
      duration: 30*60*1000, //duration in milliseconds
      activeDuration: 5*60*1000, //amout of time to extend the session upon subsequent requests
      httpOnly: true, //doesn't allow js to see this cookie on the front end
      secure: false, //use only https, normally this should be true, but we are on heroku so things work alittle differently
      ephemral: true //deletes the cookie when the brouser is closed
    }));

    //a sample route
    app.get('/test', routes.test);
    app.get('/search', routes.search);

    app.get('/api/home', routes.home);
    app.get('/api/account', routes.getAccount);

    app.post('/api/signUp', routes.signUpRequest);
    app.post('/api/signIn', routes.signInRequest);
    app.get('/api/signOff', routes.signOffRequest);
    app.get('/account', routes.accountPage);

    app.get('/api/requests', routes.requests);
    app.get('/api/noOnesTeams', routes.noOnesTeams);
    app.post('/api/request/make', routes.makeRequest);
    app.post('/api/request/respond', routes.respondToRequest);

    app.get('/signIn', routes.signInPage);
    app.get('/signUp', routes.signUpPage);

    app.get('/team/:teamName', routes.team);
    app.get('/player/:playerId', routes.player);
    app.get('/game/:gameId', routes.game);

    app.get('/api/team/:teamName', routes.teamApi);
    app.get('/api/player/:playerId', routes.playerApi);
    app.get('/api/game/:gameId', routes.gameApi);

    app.post('/api/game', routes.addGame);
    app.post('/api/player', routes.addPlayer);
    app.get('/api/teams', routes.getTeams);
    app.get('/addgame', routes.addGamePage);

    app.get('*', routes.error); //404 error

    app.listen(process.env.PORT || 4389); //last 4 digets of my student number hopuflly it's a free port
    console.log('Listening on port ' + (process.env.PORT || 4389));
}
