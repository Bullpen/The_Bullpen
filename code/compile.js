var less = require('less');
var fs = require('fs');

var lessRootDirectory = './Less/';
var outputCssDirectory = './assets/css/style.css';

var currentLessString = '';

exports.compile = function (callback) {
    //get all the files in a directory
    fs.readdir(lessRootDirectory, function(err, files) {
    if (err) {
        return console.error(err);
    }

    //for each file in directory, read it and save it to a string
    files.forEach(function (file, index, array) {
        //read the file
            fs.readFile(lessRootDirectory + file, function (err, data) {
                if (err) {
                    return console.error(err);
                }

                currentLessString += data.toString();

                //if the last file was read, render the less, and save to file
                if(index == array.length - 1) {
                    renderLess();
                }
            });
        });
    });

    function renderLess() {
        less.render(currentLessString, {compress: true}, function (e, output) {
            if (e) {
                return console.error(e);
            }

            fs.writeFile(outputCssDirectory, output.css,  function(err) {
                console.log('css is upto date');
                callback();
            });
        });
    }
}
