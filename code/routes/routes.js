var path = require('path');
var pg = require('pg');
var url = require('url');
var security = require('./security');

var params = url.parse(process.env.BULLPEN_PSQL_DB);
var auth = params.auth.split(':');
var config = {
  user: auth[0],
  password: auth[1],
  host: params.hostname,
  port: params.port,
  database: params.pathname.split('/')[1],
  ssl: true,
  max: 10, //max number of clients
  idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
};

var pool = new pg.Pool(config);

exports.test = function (req, res) {
  res.send('sample request');
};

exports.search = function (req, res) {
  if(! (/^([a-zA-Z ]+)$/.test(req.query.q)) ) {
    console.log('here');
    res.send(JSON.stringify({error : {message: "There are no results that match your search."}}));
  }

  pool.query('SELECT teamName FROM the_bullpen.Team WHERE LOWER(teamName) = LOWER($1) LIMIT 1; ', [req.query.q], function (err, queryRes) {
    if(err) {
      console.log(err);
      return res.status(500).end();
    }

    if(queryRes.rows[0]) {
      res.send(JSON.stringify({url: "/team/" + queryRes.rows[0].teamname}));
    }
    else {
      var nameArray = req.query.q.split(' ');
      var name = nameArray[nameArray.length-1];
      pool.query('SELECT playerId FROM the_bullpen.Player WHERE LOWER(lastName) = LOWER($1) LIMIT 1; ', [name], function (err, queryRes) {
        if(err) {
          console.log(err);
          return res.status(500).end();
        }

        if(queryRes.rows[0]) {
          res.send(JSON.stringify({url: "/player/" + queryRes.rows[0].playerid}));
        }
        else {
          res.send(JSON.stringify({error : {message: "There are no results that match your search."}}));
        }
      });
    }
  });
};

exports.home = function (req, res) {
  var query = 'SELECT gameId, homeTeam, awayTeam, winner, gameTime, homeScore, awayScore ' +
              'FROM the_bullpen.Game g ' +
              'ORDER BY g.gameTime DESC ' +
              'LIMIT 5;';

  var query1 = 'SELECT t.teamName, (SELECT count(*) FROM the_bullpen.Game g WHERE g.winner = t.teamName) AS wins ' +
              'FROM the_bullpen.Team t  ' +
              'ORDER BY wins DESC ' +
              'limit 5;';

  pool.query(query, null, function (err, queryRes) {
    if(err) {
      console.log(err);
      return res.status(500).end();
    }

    pool.query(query1, null, function (err1, queryRes1) {
      if(err1) {
        console.log(err1);
        return res.status(500).end();
      }

      responce = {};
      responce.recentGames = [];
      for(var i = 0; i < queryRes.rows.length; i++) {
        var game = {};
        game.gameId = queryRes.rows[i].gameid;
        game.homeTeam = queryRes.rows[i].hometeam;
        game.awayTeam = queryRes.rows[i].awayteam;
        game.gameTime = queryRes.rows[i].gametime;
        game.homeScore = queryRes.rows[i].homescore;
        game.awayScore = queryRes.rows[i].awayscore;
        responce.recentGames.push(game);
      }

      responce.topTeams = [];
      for(var i = 0; i < queryRes1.rows.length; i++) {
        var team  = {};
        team.teamName = queryRes1.rows[i].teamname;
        team.wins = queryRes1.rows[i].wins;
        responce.topTeams.push(team);
      }

      res.send(responce);
    });
  });
};

exports.signUpRequest = function(req, res) {
  if(!req.body.username || !req.body.password) {
    return res.status(400).end();
  }
  else if((!(/^([a-zA-Z0-9]+)$/.test(req.body.username))) || (!(/^([a-zA-Z0-9]+)$/.test(req.body.password))) ||
            (req.body.email && (!(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/.test(req.body.email)))) ) {
    return res.status(400).end();
  }

  var salt = security.getSalt();
  var hashedPass = security.getHash(req.body.password, salt);

  var emailQuery = 'INSERT INTO the_bullpen.Account(username, password, salt, email, accountType) VALUES ($1::text, $2::text, $3::text, $4::text, \'basic\') RETURNING accountId; ';
  var emailValues = [req.body.username, hashedPass, salt, req.body.email];

  var noEmailQuery = 'INSERT INTO the_bullpen.Account(username, password, salt, accountType) VALUES ($1::text, $2::text, $3::text, \'basic\') RETURNING accountId; ';
  var noEmailValues = [req.body.username, hashedPass, salt];

  pool.query((req.body.email)? emailQuery: noEmailQuery , (req.body.email)? emailValues: noEmailValues, function (err, queryRes) {
    if(err) {
      console.log(err);
      return res.status(500).end();
    }

    if(queryRes.rowCount == 1 && queryRes.rows[0]) {
      req.session.user = queryRes.rows[0].accountid;
      return res.status(200).end();
    }
    else {
      return res.status(400).end();
    }
  });
};

exports.signInRequest = function(req, res) {
  if(!req.body.username || !req.body.password) {
    return res.status(400).end();
  }
  else if((!(/^([a-zA-Z0-9]+)$/.test(req.body.username))) || (!(/^([a-zA-Z0-9]+)$/.test(req.body.password))) ) {
    return res.status(400).end();
  }

  pool.query('SELECT accountId, password, salt FROM the_bullpen.Account WHERE username = $1::text; ', [req.body.username], function (err, queryRes) {
    if(err) {
      console.log(err);
      return res.status(500).end();
    }

    if(queryRes.rows[0]) {
      if(security.verify(req.body.password, queryRes.rows[0].salt, queryRes.rows[0].password)) {
        req.session.user = queryRes.rows[0].accountid;
        return res.status(200).end();
      } else {
        return res.status(400).end();
      }
    }
    else {
      return res.status(400).end();
    }
  });
};

exports.signOffRequest = function(req, res) {
  req.session.reset();
  res.status(200).end();
};

exports.signInPage  = function(req, res) {
  if(!req.session.user)
    res.sendFile(path.resolve('dinamicHtml/signIn.html'));
  else
    res.redirect(302, '/');
};

exports.signUpPage  = function(req, res) {
  if(!req.session.user)
    res.sendFile(path.resolve('dinamicHtml/signUp.html'));
  else
    res.redirect(302, '/');
};

exports.getAccount = function(req, res) {
  var responce = {};

  if(!req.session.user) {
    return res.send({hasAccount : false});
  }

  query = 'SELECT a.username, a.accountType, t.teamName AS managerOf, ur.requestId, ur.requestTime, ur.upgradeTo, ur.teamName, ur.processor, ur.processTime, ur.accepted, ura.username AS processorName ' +
          'FROM the_bullpen.Account a LEFT OUTER JOIN the_bullpen.Team t ON a.accountId = t.managerId ' +
                'LEFT OUTER JOIN the_bullpen.upgradeRequests ur ON ur.accountId = a.accountId ' +
                'LEFT OUTER JOIN the_bullpen.Account ura ON ur.processor = ura.accountId ' +
          'WHERE a.accountId = $1 ' +
          'ORDER BY ur.requestTime DESC ' +
          'LIMIT 1;';

  pool.query(query, [req.session.user], function (err, queryRes) {
    if(err) {
      console.log(err);
      res.status(500).end();
    }

    if(queryRes.rows.length == 1) {
      res.send({
        hasAccount : true,
        name : queryRes.rows[0].username,
        accountType : queryRes.rows[0].accounttype,
        team : queryRes.rows[0].managerof,
        request: {
          requestId: queryRes.rows[0].requestid,
          time: queryRes.rows[0].requesttime,
          upgradeTo: queryRes.rows[0].upgradeto,
          teamName: queryRes.rows[0].teamname,
          processor: queryRes.rows[0].processor,
          processTime: queryRes.rows[0].processtime,
          acceptStatus: queryRes.rows[0].accepted,
          username: queryRes.rows[0].processorname
        }
      });
    } else {
      req.session.reset();
      res.send({hasAccount : false});
    }
  });
};

exports.accountPage = function(req, res) {
  if(req.session.user) {
    res.sendFile(path.resolve('dinamicHtml/account.html'));
  } else {
    res.redirect(302, '/signIn');
  }
}


exports.requests = function(req, res) {
  if(!req.session.user) {
    return res.status(404).end();
  }

  query = 'SELECT 1 FROM the_bullpen.Account WHERE accountId = $1 AND accountType = \'admin\';';
  pool.query(query, [req.session.user], function (err, queryRes) {
    if(err) {
      console.log(err);
      res.status(500).end();
    }

    if(queryRes.rows.length == 0) {
      return res.status(404).end();
    }

    query1 = 'SELECT ur.requestId, ur.accountId, ur.requestTime, ur.upgradeTo, ur.teamName, a.username FROM the_bullpen.upgradeRequests ur JOIN the_bullpen.Account a ON a.accountId = ur.accountId WHERE processor IS NULL;';
    pool.query(query1, null, function (err1, queryRes1) {
      if(err1) {
        console.log(err1);
        return res.status(500).end();
      }

      var responce = [];
      for(var i = 0; i < queryRes1.rows.length; i++) {
        var request = {};
        request.requestId = queryRes1.rows[i].requestid;
        request.accountId = queryRes1.rows[i].accountid;
        request.username = queryRes1.rows[i].username;
        request.requestTime = queryRes1.rows[i].requesttime;
        request.upgradeTo = queryRes1.rows[i].upgradeto;
        request.teamName = queryRes1.rows[i].teamname;
        responce.push(request);
      }

      res.send(responce);
    });

  });

}

exports.noOnesTeams = function(req, res) {
  pool.query('SELECt teamName FROM the_bullpen.Team WHERE managerId IS NULL;', null, function (err, queryRes) {
    if(err) {
      console.log(err);
      return res.status(500).end();
    }

    var responce = [];
    for(var i = 0; i < queryRes.rows.length; i++) {
      responce.push(queryRes.rows[i].teamname);
    }

    res.send(responce);
  });
};

exports.makeRequest = function(req, res) {
  if(!req.session.user) {
    return res.status(404).end();
  }

  if(typeof req.body.upgradeTo !== 'string' || (req.body.upgradeTo !== 'admin' && req.body.upgradeTo !== 'manager')) {
    return res.status(400).end();
  }

  if(req.body.upgradeTo === 'manager' && (typeof req.body.team !== 'string' || !/^([a-zA-Z]+)$/.test(req.body.team))) {
    return res.status(400).end();
  }

  var manager = (req.body.upgradeTo === 'manager');

  queryNm = 'SELECT 1 ' +
            'FROM the_bullpen.Account a ' +
            'WHERE a.accountId = $1 AND a.accountType = \'basic\';';

  querym = 'SELECT 1 ' +
           'FROM the_bullpen.Account a FULL OUTER JOIN the_bullpen.Team t ON true ' +
           'WHERE a.accountId = $1 AND a.accountType = \'basic\' AND t.teamName = $2 AND t.managerId IS NULL;';

  var values = [];
  values.push(req.session.user);
  (manager)? values.push(req.body.team): '';

  pool.query((manager)? querym: queryNm, values, function (err, queryRes) {
    if(err) {
      console.log(err);
      return res.status(500).end();
    }

    if(queryRes.rows.length === 0) {
      return res.status(400).end();
    }

    query1Nm = 'INSERT INTO the_bullpen.upgradeRequests(accountId, upgradeTo) VALUES($1, \'admin\');';

    query1m = 'INSERT INTO the_bullpen.upgradeRequests(accountId, upgradeTo, teamName) VALUES($1, \'manager\', $2);';

    pool.query((manager)? query1m: query1Nm, values, function (err1, queryRes1) {
      if(err1) {
        console.log(err1);
        return res.status(500).end();
      }

      res.status(200).end();
    });
  });
}

exports.respondToRequest = function(req, res) {
  if(!req.session.user) {
    return res.status(404).end();
  }

  if(typeof req.body.status !== "boolean" || typeof req.body.requestId !== 'number') {
    return res.status(400).end();
  }

  pool.query('SELECT 1 FROM the_bullpen.Account WHERE accountId = $1 AND accountType = \'admin\';', [req.session.user], function (err, queryRes) {
    if(err) {
      console.log(err);
      return res.status(500).end();
    }

    if(queryRes.rows.length == 0) {
      return res.status(404).end();
    }

    var query = 'SELECT the_bullpen.request_responce($1::integer, $2::boolean, $3::integer);';
    pool.query(query, [req.session.user, req.body.status, req.body.requestId], function (err1, queryRes1) {
      if(err1) {
        console.log(err1);
        res.status(500).end();
      }

      res.status(200).end();
    });
  });
}


exports.team = function(req, res) {
  if(! (/^([a-zA-Z]+)$/.test(req.params.teamName)) ) {
    return exports.error(req, res);
  }

  pool.query('SELECT 1 FROM the_bullpen.Team WHERE teamName = $1::text ', [req.params.teamName], function (err, queryRes) {
    if(err) {
      console.log(err);
      return res.status(500).end();
    }

    if(queryRes.rows[0]) {
      res.sendFile(path.resolve('dinamicHtml/team.html'));
    }
    else {
      return exports.error(req, res);
    }
  });
};

exports.player = function(req, res) {
  if(! (/^([0-9]+)$/.test(req.params.playerId)) ) {
    return exports.error(req, res);
  }

  pool.query('SELECT 1 FROM the_bullpen.Player WHERE playerId = $1::integer ', [req.params.playerId], function (err, queryRes) {
    if(err) {
      console.log(err);
      return res.status(500).end();
    }

    if(queryRes.rows[0]) {
      res.sendFile(path.resolve('dinamicHtml/player.html'));
    }
    else {
      return exports.error(req, res);
    }
  });
};

exports.game = function(req, res) {
  if(! (/^([0-9]+)$/.test(req.params.gameId)) ) {
    return exports.error(req, res);
  }

  pool.query('SELECT 1 FROM the_bullpen.Game WHERE gameId = $1::integer ', [req.params.gameId], function (err, queryRes) {
    if(err) {
      console.log(err);
      return res.status(500).end();
    }

    if(queryRes.rows[0]) {
      res.sendFile(path.resolve('dinamicHtml/game.html'));
    }
    else {
      return exports.error(req, res);
    }
  });
};

exports.teamApi = function(req, res) {
  if(! (/^([a-zA-Z]+)$/.test(req.params.teamName)) ) {
    return exports.error(req, res);
  }

  var recordQuery = 'SELECT t.teamName, t.homefield, t.mascot, t.logo, t.managerId, a.username, COALESCE(count(CASE WHEN g.winner = t.teamName THEN 1 END), 0)::int AS wins, COALESCE(count(CASE WHEN g.winner != t.teamName THEN 1 END), 0)::int AS losses ' +
                    'FROM the_bullpen.Team t LEFT OUTER JOIN the_bullpen.Game g ON t.teamName = g.homeTeam OR t.teamName = g.awayTeam ' +
                                'LEFT OUTER JOIN the_bullpen.Account a ON a.accountId = t.managerId ' +
                    'WHERE t.teamName = $1::text ' +
                    'GROUP BY t.teamName, t.homefield, t.mascot, t.logo, t.managerId, a.username;';

  pool.query(recordQuery, [req.params.teamName], function (recordErr, recordRes) {
    if(recordErr) {
      console.log(recordErr);
      return res.status(500).end();
    }

    if(recordRes.rows[0]) {
      var gamesQuery = 'SELECT g.gameId, g.gameTime, g.homeTeam, g.awayTeam, g.homeScore, g.awayScore ' +
                        'FROM the_bullpen.Game g ' +
                        'WHERE g.homeTeam = $1::text OR g.awayTeam = $1::text ' +
                        'ORDER BY g.gameTime DESC;';

      pool.query(gamesQuery, [req.params.teamName], function (gamesErr, gamesRes) {
        if(gamesErr) {
          console.log(gamesErr);
          return res.status(500).end();
        }

        if(gamesRes.rows[0]) {
          var playersQuery = 'SELECT p.playerId, p.firstName || \' \' || p.lastName AS name, p.num ' +
                            'FROM the_bullpen.Player p ' +
                            'WHERE p.team = $1::text ' +
                            'ORDER BY p.num;';

          pool.query(playersQuery, [req.params.teamName], function (playersErr, playersRes) {
            if(playersErr) {
              console.log(playersErr);
              return res.status(500).end();
            }

            if(playersRes.rows[0]) {
              var responce = {};

              responce.teamname = recordRes.rows[0].teamname;
              (recordRes.rows[0].homefield)? responce.homefield = recordRes.rows[0].homefield: '';
              (recordRes.rows[0].mascot)? responce.mascot = recordRes.rows[0].mascot: '';
              (recordRes.rows[0].logo)? responce.logo = recordRes.rows[0].logo: '';
              (recordRes.rows[0].managerid)? responce.managerId = recordRes.rows[0].managerid: '';
              (recordRes.rows[0].username)? responce.managerUsername = recordRes.rows[0].username: '';
              responce.wins = recordRes.rows[0].wins;
              responce.losses = recordRes.rows[0].losses;

              responce.games = [];
              for(var i = 0; i < gamesRes.rows.length; i++) {
                var game = {};
                game.gameId = gamesRes.rows[i].gameid;
                game.gameTime = gamesRes.rows[i].gametime;
                game.homeTeam = gamesRes.rows[i].hometeam;
                game.awayTeam = gamesRes.rows[i].awayteam;
                game.homeScore = gamesRes.rows[i].homescore;
                game.awayScore = gamesRes.rows[i].awayscore;
                responce.games.push(game);
              }

              responce.players = [];
              for(var i = 0; i < playersRes.rows.length; i++) {
                var player = {};
                player.playerId = playersRes.rows[i].playerid;
                player.name = playersRes.rows[i].name;
                player.num = playersRes.rows[i].num;
                responce.players.push(player);
              }

              res.send(responce);
            } else {
              return exports.error(req, res);
            }
          });
        } else {
          return exports.error(req, res);
        }
      });
    } else {
      return exports.error(req, res);
    }
  });
};

exports.playerApi = function(req, res) {
  if(! (/^([0-9]+)$/.test(req.params.playerId)) ) {
    return exports.error(req, res);
  }

  var queryString = 'SELECT p.playerId, p.team, p.firstName || \' \' || p.lastName AS name, p.num, g.gameId, g.gameTime, g.homeTeam, g.awayTeam, g.winner, g.homeScore, g.awayScore, p.B1, p.B2, p.B3, p.HR, p.H, p.uBB, p.iBB, p.BB, p.SF, p.HBP, p.k, p.G, p.PA, p.AB, p.R, p.RBI, p.SB, p.CS, p.SBp, p.BBp, p.Kp, p.bAVG, p.BABIP, p.OBP, p.wOBA, p.wRCplus, p.Wpitcher, p.Lpitcher, p.SVpitcher, p.Gpitcher, p.GSpitcher, p.IPpitcher, p.IPfracpitcher, p.Hpitcher, p.Kpitcher, p.BBpitcher, p.HRpitcher, p.Rpitcher, p.ERpitcher, p.HBPpitcher, p.Kper9pitcher, p.BBper9pitcher, p.ERApitcher, p.FIPpitcher ' +
                    'FROM the_bullpen.Player p JOIN the_bullpen.Team t ON p.team = t.teamName ' +
                                              'LEFT OUTER JOIN the_bullpen.Game g ON g.homeTeam = t.teamName OR g.awayTeam = t.teamName ' +
                    'WHERE playerId = $1::integer ' +
                    'ORDER BY g.gameTime DESC ' +
                    'LIMIT 5; '

  pool.query(queryString, [req.params.playerId], function (err, queryRes) {
    if(err) {
      console.log(err);
      return res.status(500).end();
    }

    if(queryRes.rows[0]) {
      //make send back info about the player they requested
      var responce = {};

      responce.playerId = queryRes.rows[0].playerid;
      responce.team = queryRes.rows[0].team;
      responce.name = queryRes.rows[0].name;
      responce.num = queryRes.rows[0].num;

      responce.recentGames = [];
      for(var i = 0; i < queryRes.rows.length; i++) {
        if(queryRes.rows[i].gameid) {
          var game = {};
          game.gameId = queryRes.rows[i].gameid;
          game.gameTime = queryRes.rows[i].gametime;
          game.homeTeam = queryRes.rows[i].hometeam;
          game.awayTeam = queryRes.rows[i].awayteam;
          game.winner = queryRes.rows[i].winner;
          game.homeScore = queryRes.rows[i].homescore;
          game.awayScore = queryRes.rows[i].awayscore;
          responce.recentGames.push(game);
        }
      }

      responce.stats = queryRes.rows[0];
      delete responce.stats.gameid;
      delete responce.stats.gametime;
      delete responce.stats.playerid;
      delete responce.stats.hometeam;
      delete responce.stats.awayteam;
      delete responce.stats.winner;
      delete responce.stats.homescore;
      delete responce.stats.awayscore;
      delete responce.stats.team;
      delete responce.stats.name;
      delete responce.stats.num;

      res.send(responce);
    }
    else {
      return exports.error(req, res);
    }
  });
};

exports.gameApi = function(req, res) {
  if(! (/^([0-9]+)$/.test(req.params.gameId)) ) {
    return exports.error(req, res);
  }

  var queryString = 'SELECT g.gameId, g.homeTeam, g.awayTeam, g.gameTime, g.numInnings, g.winner, g.homeScore, g.awayScore, g.homeHits, g.awayHits, g.winningPitcher, g.losingPitcher, g.savePitcher, g.homeStarter, g.awayStarter, pgo.player, p.team, p.firstName || \' \' || p.lastName AS name, pgo.B1, pgo.B2, pgo.B3, pgo.HR, pgo.H, pgo.uBB, pgo.iBB, pgo.BB, pgo.SF, pgo.HBP, pgo.K, pgo.PA, pgo.AB, pgo.R, pgo.RBI, pgo.SB, pgo.CS ' +
                    'FROM the_bullpen.Game g LEFT OUTER JOIN the_bullpen.player_game_offence pgo ON g.gameId = pgo.game ' +
                                            'JOIN the_bullpen.Player p ON pgo.player = p.playerId ' +
                    'WHERE g.gameId = $1::integer ';

  pool.query(queryString, [req.params.gameId], function (errOffence, queryResOffence) {
    if(errOffence) {
      console.log(errOffence);
      return res.status(500).end();
    }

    if(queryResOffence.rows[0]) {

      var queryString = 'SELECT pgd.player, p.firstName || \' \' || p.lastName AS name, p.team, pgd.IP, pgd.IPfrac, pgd.H, pgd.K, pgd.BB, pgd.HR, pgd.R, pgd.ER, pgd.HBP ' +
                        'FROM the_bullpen.Game g LEFT OUTER JOIN the_bullpen.player_game_defence pgd ON g.gameId = pgd.game ' +
                                                'JOIN the_bullpen.Player p ON pgd.player = p.playerId ' +
                        'WHERE g.gameId = $1::integer ';

      pool.query(queryString, [req.params.gameId], function (errDefence, queryResDefence) {
        if(errDefence) {
          console.log(errDefence);
          return res.status(500).end();
        }

        if(queryResDefence.rows[0]) {
          //make send back info about the player they requested
          var responce = {};

          responce.homeTeam = queryResOffence.rows[0].hometeam;
          responce.awayTeam = queryResOffence.rows[0].awayteam;
          responce.gameTime = queryResOffence.rows[0].gametime;
          responce.numInnings = queryResOffence.rows[0].numinnings;
          responce.winner = queryResOffence.rows[0].winner;
          responce.homeScore = queryResOffence.rows[0].homescore;
          responce.awayScore = queryResOffence.rows[0].awayscore;
          responce.homeHits = queryResOffence.rows[0].homehits;
          responce.awayHits = queryResOffence.rows[0].awayhits;
          responce.winningPitcher = queryResOffence.rows[0].winningpitcher;
          responce.losingPitcher = queryResOffence.rows[0].losingpitcher;
          responce.homeStarter = queryResOffence.rows[0].homestarter;
          responce.awayStarter = queryResOffence.rows[0].awaystarter;
          (queryResOffence.rows[0].savepitcher)? responce.savePitcher = queryResOffence.rows[0].savepitcher: '';

          responce.homePlayers = {};
          responce.awayPlayers = {};
          responce.homePlayers.offensive = [];
          responce.homePlayers.defensive = [];
          responce.awayPlayers.offensive = [];
          responce.awayPlayers.defensive = [];

          for(var i = 0; i < queryResOffence.rows.length; i++) {
            var player = {};
            player.playerId = queryResOffence.rows[i].player;
            player.name = queryResOffence.rows[i].name;
            player.stats = {};
            player.stats.B1 = queryResOffence.rows[i].b1;
            player.stats.B2 = queryResOffence.rows[i].b2;
            player.stats.B3 = queryResOffence.rows[i].b3;
            player.stats.HR = queryResOffence.rows[i].hr;
            player.stats.H = queryResOffence.rows[i].h;
            player.stats.uBB = queryResOffence.rows[i].ubb;
            player.stats.iBB = queryResOffence.rows[i].ibb;
            player.stats.BB = queryResOffence.rows[i].bb;
            player.stats.SF = queryResOffence.rows[i].sf;
            player.stats.HBP = queryResOffence.rows[i].hbp;
            player.stats.K = queryResOffence.rows[i].k;
            player.stats.PA = queryResOffence.rows[i].pa;
            player.stats.AB = queryResOffence.rows[i].ab;
            player.stats.R = queryResOffence.rows[i].r;
            player.stats.RBI = queryResOffence.rows[i].rbi;
            player.stats.SB = queryResOffence.rows[i].sb;
            player.stats.CS = queryResOffence.rows[i].cs;

            if(queryResOffence.rows[i].team == responce.homeTeam) {
              responce.homePlayers.offensive.push(player);
            } else {
              responce.awayPlayers.offensive.push(player);
            }
          }

          for(var i = 0; i < queryResDefence.rows.length; i++) {
            var player = {};
            player.playerId = queryResDefence.rows[i].player;
            player.name = queryResDefence.rows[i].name;
            player.stats = {};
            player.stats.IP = queryResDefence.rows[i].ip;
            player.stats.IPfrac = queryResDefence.rows[i].ipfrac;
            player.stats.H = queryResDefence.rows[i].h;
            player.stats.K = queryResDefence.rows[i].k;
            player.stats.BB = queryResDefence.rows[i].bb;
            player.stats.HR = queryResDefence.rows[i].hr;
            player.stats.R = queryResDefence.rows[i].r;
            player.stats.ER = queryResDefence.rows[i].er;
            player.stats.HBP = queryResDefence.rows[i].hbp;

            if(queryResDefence.rows[i].team == responce.homeTeam) {
              responce.homePlayers.defensive.push(player);
            } else {
              responce.awayPlayers.defensive.push(player);
            }
          }

          res.send(responce);
        }
        else {
          return exports.error(req, res);
        }
      });
    }
    else {
      return exports.error(req, res);
    }
  });
};


exports.addGame = function(req, res) {
  if(!req.session.user) {
    return res.status(400).end();
  }

  if(/^([a-zA-Z]+)$/.test(req.body.homeTeam) && /^([a-zA-Z]+)$/.test(req.body.awayTeam) && typeof req.body.numInnings === "number" && (req.body.winner === req.body.homeTeam || req.body.winner === req.body.awayTeam) &&
     typeof req.body.homeScore === "number" && typeof req.body.awayScore === "number" && typeof req.body.homeHits === "number" && typeof req.body.awayHits === "number" && req.body.homePlayers && req.body.awayPlayers &&
     Array.isArray(req.body.homePlayers.offensive) && Array.isArray(req.body.homePlayers.defensive) && Array.isArray(req.body.awayPlayers.offensive) && Array.isArray(req.body.awayPlayers.defensive) &&
     req.body.homePlayers.offensive.length >= 1 && req.body.homePlayers.defensive.length >= 1 && req.body.awayPlayers.offensive.length >= 1 && req.body.awayPlayers.defensive.length >= 1) {

    var winning = -1;
    var loosing = -1;
    var save = -1;
    var homeStarter = -1;
    var awayStarter = -1;
    for(var i = 0; i < req.body.homePlayers.offensive.length; i++) {
      var test = req.body.homePlayers.offensive[i];
      if(typeof test.playerId === "number" && test.stats && typeof test.stats.B1 === "number" && typeof test.stats.B2 === "number" && typeof test.stats.B3 === "number" &&
         typeof test.stats.HR === "number" && typeof test.stats.uBB === "number" && typeof test.stats.iBB === "number" && typeof test.stats.SF === "number" &&
         typeof test.stats.HBP === "number" && typeof test.stats.K === "number" && typeof test.stats.PA === "number" && typeof test.stats.AB === "number" &&
         typeof test.stats.R === "number" && typeof test.stats.RBI === "number" && typeof test.stats.SB === "number" && typeof test.stats.CS === "number") {
          ;//all good here
      }
      else {
        return res.status(400).end();
      }
    }
    for(var i = 0; i < req.body.awayPlayers.offensive.length; i++) {
      var test = req.body.awayPlayers.offensive[i];
      if(typeof test.playerId === "number" && test.stats && typeof test.stats.B1 === "number" && typeof test.stats.B2 === "number" && typeof test.stats.B3 === "number" &&
         typeof test.stats.HR === "number" && typeof test.stats.uBB === "number" && typeof test.stats.iBB === "number" && typeof test.stats.SF === "number" &&
         typeof test.stats.HBP === "number" && typeof test.stats.K === "number" && typeof test.stats.PA === "number" && typeof test.stats.AB === "number" &&
         typeof test.stats.R === "number" && typeof test.stats.RBI === "number" && typeof test.stats.SB === "number" && typeof test.stats.CS === "number") {
            ;//all good here
      }
      else {
        return res.status(400).end();
      }
    }
    for(var i = 0; i < req.body.homePlayers.defensive.length; i++) {
      var test = req.body.homePlayers.defensive[i];
      if(typeof test.playerId === "number" && test.stats && typeof test.stats.IP === "number" && typeof test.stats.IPfrac === "number" &&
         typeof test.stats.H === "number" && typeof test.stats.K === "number" && typeof test.stats.BB === "number" && typeof test.stats.HR === "number" &&
         typeof test.stats.R === "number" && typeof test.stats.ER === "number" && typeof test.stats.HBP === "number") {
           if(test.winner) {
             if(winning == -1 && (req.body.homeTeam == req.body.winner)) {
               winning = test.playerId;
             } else {
               return res.status(400).end();
             }
           }
           else if(test.save) {
             if(save == -1 && (req.body.homeTeam == req.body.winner)) {
               save = test.playerId;
             } else {
               return res.status(400).end();
             }
           }
           else if(test.looser) {
             if((loosing == -1) && (req.body.homeTeam != req.body.winner)) {
               loosing = test.playerId;
             } else {
               return res.status(400).end();
             }
           }

           if(test.starter) {
             if(homeStarter == -1) {
               homeStarter = test.playerId;
             } else {
               return res.status(400).end();
             }
           }
      }
      else {
        return res.status(400).end();
      }
    }
    for(var i = 0; i < req.body.awayPlayers.defensive.length; i++) {
      var test = req.body.awayPlayers.defensive[i];
      if(typeof test.playerId === "number" && test.stats && typeof test.stats.IP === "number" && typeof test.stats.IPfrac === "number" &&
         typeof test.stats.H === "number" && typeof test.stats.K === "number" && typeof test.stats.BB === "number" && typeof test.stats.HR === "number" &&
         typeof test.stats.R === "number" && typeof test.stats.ER === "number" && typeof test.stats.HBP === "number") {
           if(test.winner) {
             if(winning == -1 && req.body.awayTeam == req.body.winner) {
               winning = test.playerId;
             } else {
               return res.status(400).end();
             }
           }
           else if(test.save) {
             if(save == -1 && req.body.awayTeam == req.body.winner) {
               save = test.playerId;
             } else {
               return res.status(400).end();
             }
           }
           else if(test.looser) {
             if(loosing == -1 && req.body.awayTeam != req.body.winner) {
               loosing = test.playerId;
             } else {
               return res.status(400).end();
             }
           }

           if(test.starter) {
             if(awayStarter == -1) {
               awayStarter = test.playerId;
             } else {
               return res.status(400).end();
             }
           }
      }
      else {
        return res.status(400).end();
      }
    }

    if(winning == -1 || loosing == -1 || homeStarter == -1 || awayStarter == -1) {
      return res.status(400).end();
    }

    //ready to start inputing data
    console.log('went through the validation');

    var testUserQuery = 'SELECT 1 ' +
                        'FROM the_bullpen.Account a LEFT OUTER JOIN the_bullpen.Team t ON a.accountId = t.managerId ' +
                        'WHERE a.accountId = $1 AND (accountType = \'admin\' OR (accountType = \'manager\' AND t.teamName = $2::text ) ); ';

    pool.query(testUserQuery, [req.session.user, req.body.homeTeam], function(testUserErr, testUserResponce) {
      if(testUserErr) {
        console.log(testUserErr);
        res.status(500).end();
      }

      var query = '';
      //add the game
      query += 'WITH newGameId AS (INSERT INTO the_bullpen.Game(homeTeam, awayTeam, gameTime, numInnings, winner, homeScore, awayScore, homeHits, awayHits) ' +
      		                'VALUES ($1::text, $2::text, now(), $3::int, ' + ((req.body.winner == req.body.homeTeam)? '$1': '$2') + '::text, $4::int, $5::int, $6::int, $7::int) RETURNING gameId), ';
      content = [req.body.homeTeam, req.body.awayTeam, req.body.numInnings, req.body.homeScore, req.body.awayScore, req.body.homeHits, req.body.awayHits];

      //add offensive players
      query += 'pgo AS (INSERT INTO the_bullpen.player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS) VALUES ';
      for(var i = 0; i < req.body.homePlayers.offensive.length; i++) {
        query += '($' + (content.length + 1) + '::int, (SELECT gameId FROM newGameId)::int, $' + (content.length + 2) + '::int, $' + (content.length + 3) + '::int, ' +
                 ' $' + (content.length + 4) + '::int, $' + (content.length + 5) + '::int, $' + (content.length + 6) + '::int, $' + (content.length + 7) + '::int, ' +
                 ' $' + (content.length + 8) + '::int, $' + (content.length + 9) + '::int, $' + (content.length + 10) + '::int, $' + (content.length + 11) + '::int, ' +
                 ' $' + (content.length + 12) + '::int, $' + (content.length + 13) + '::int, $' + (content.length + 14) + '::int, $' + (content.length + 15) + '::int, ' +
                 ' $' + (content.length + 16) + '::int, $' + (content.length + 17) + '::int, $' + (content.length + 18) + '::int), ';

        content.push(req.body.homePlayers.offensive[i].playerId);
        content.push(req.body.homePlayers.offensive[i].stats.B1);
        content.push(req.body.homePlayers.offensive[i].stats.B2);
        content.push(req.body.homePlayers.offensive[i].stats.B3);
        content.push(req.body.homePlayers.offensive[i].stats.HR);
        content.push(req.body.homePlayers.offensive[i].stats.B1 + req.body.homePlayers.offensive[i].stats.B2 + req.body.homePlayers.offensive[i].stats.B3 + req.body.homePlayers.offensive[i].stats.HR);
        content.push(req.body.homePlayers.offensive[i].stats.uBB);
        content.push(req.body.homePlayers.offensive[i].stats.iBB);
        content.push(req.body.homePlayers.offensive[i].stats.uBB + req.body.homePlayers.offensive[i].stats.iBB);
        content.push(req.body.homePlayers.offensive[i].stats.SF);
        content.push(req.body.homePlayers.offensive[i].stats.HBP);
        content.push(req.body.homePlayers.offensive[i].stats.K);
        content.push(req.body.homePlayers.offensive[i].stats.PA);
        content.push(req.body.homePlayers.offensive[i].stats.AB);
        content.push(req.body.homePlayers.offensive[i].stats.R);
        content.push(req.body.homePlayers.offensive[i].stats.RBI);
        content.push(req.body.homePlayers.offensive[i].stats.SB);
        content.push(req.body.homePlayers.offensive[i].stats.CS);
      }
      for(var i = 0; i < req.body.awayPlayers.offensive.length; i++) {
        query += '($' + (content.length + 1) + '::int, (SELECT gameId FROM newGameId)::int, $' + (content.length + 2) + '::int, $' + (content.length + 3) + '::int, ' +
                 ' $' + (content.length + 4) + '::int, $' + (content.length + 5) + '::int, $' + (content.length + 6) + '::int, $' + (content.length + 7) + '::int, ' +
                 ' $' + (content.length + 8) + '::int, $' + (content.length + 9) + '::int, $' + (content.length + 10) + '::int, $' + (content.length + 11) + '::int, ' +
                 ' $' + (content.length + 12) + '::int, $' + (content.length + 13) + '::int, $' + (content.length + 14) + '::int, $' + (content.length + 15) + '::int, ' +
                 ' $' + (content.length + 16) + '::int, $' + (content.length + 17) + '::int, $' + (content.length + 18) + '::int) ';

        content.push(req.body.awayPlayers.offensive[i].playerId);
        content.push(req.body.awayPlayers.offensive[i].stats.B1);
        content.push(req.body.awayPlayers.offensive[i].stats.B2);
        content.push(req.body.awayPlayers.offensive[i].stats.B3);
        content.push(req.body.awayPlayers.offensive[i].stats.HR);
        content.push(req.body.awayPlayers.offensive[i].stats.B1 + req.body.awayPlayers.offensive[i].stats.B2 + req.body.awayPlayers.offensive[i].stats.B3 + req.body.awayPlayers.offensive[i].stats.HR);
        content.push(req.body.awayPlayers.offensive[i].stats.uBB);
        content.push(req.body.awayPlayers.offensive[i].stats.iBB);
        content.push(req.body.awayPlayers.offensive[i].stats.uBB + req.body.awayPlayers.offensive[i].stats.iBB);
        content.push(req.body.awayPlayers.offensive[i].stats.SF);
        content.push(req.body.awayPlayers.offensive[i].stats.HBP);
        content.push(req.body.awayPlayers.offensive[i].stats.K);
        content.push(req.body.awayPlayers.offensive[i].stats.PA);
        content.push(req.body.awayPlayers.offensive[i].stats.AB);
        content.push(req.body.awayPlayers.offensive[i].stats.R);
        content.push(req.body.awayPlayers.offensive[i].stats.RBI);
        content.push(req.body.awayPlayers.offensive[i].stats.SB);
        content.push(req.body.awayPlayers.offensive[i].stats.CS);

        if((i + 1) != req.body.awayPlayers.offensive.length) {
          query += ', ';
        }
      }
      query += '), ';

      //add defensive players
      query += 'pgd AS (INSERT INTO the_bullpen.player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP) VALUES ';
      for(var i = 0; i < req.body.homePlayers.defensive.length; i++) {
        query += '($' + (content.length + 1) + '::int, (SELECT gameId FROM newGameId)::int, $' + (content.length + 2) + '::int, $' + (content.length + 3) + '::int, ' +
                 ' $' + (content.length + 4) + '::int, $' + (content.length + 5) + '::int, $' + (content.length + 6) + '::int, $' + (content.length + 7) + '::int, ' +
                 ' $' + (content.length + 8) + '::int, $' + (content.length + 9) + '::int, $' + (content.length + 10) + '::int), ';

        content.push(req.body.homePlayers.defensive[i].playerId);
        content.push(req.body.homePlayers.defensive[i].stats.IP);
        content.push(req.body.homePlayers.defensive[i].stats.IPfrac);
        content.push(req.body.homePlayers.defensive[i].stats.H);
        content.push(req.body.homePlayers.defensive[i].stats.K);
        content.push(req.body.homePlayers.defensive[i].stats.BB);
        content.push(req.body.homePlayers.defensive[i].stats.HR);
        content.push(req.body.homePlayers.defensive[i].stats.R);
        content.push(req.body.homePlayers.defensive[i].stats.ER);
        content.push(req.body.homePlayers.defensive[i].stats.HBP);
      }
      for(var i = 0; i < req.body.awayPlayers.defensive.length; i++) {
        query += '($' + (content.length + 1) + '::int, (SELECT gameId FROM newGameId)::int, $' + (content.length + 2) + '::int, $' + (content.length + 3) + '::int, ' +
                 ' $' + (content.length + 4) + '::int, $' + (content.length + 5) + '::int, $' + (content.length + 6) + '::int, $' + (content.length + 7) + '::int, ' +
                 ' $' + (content.length + 8) + '::int, $' + (content.length + 9) + '::int, $' + (content.length + 10) + '::int) ';

       content.push(req.body.awayPlayers.defensive[i].playerId);
       content.push(req.body.awayPlayers.defensive[i].stats.IP);
       content.push(req.body.awayPlayers.defensive[i].stats.IPfrac);
       content.push(req.body.awayPlayers.defensive[i].stats.H);
       content.push(req.body.awayPlayers.defensive[i].stats.K);
       content.push(req.body.awayPlayers.defensive[i].stats.BB);
       content.push(req.body.awayPlayers.defensive[i].stats.HR);
       content.push(req.body.awayPlayers.defensive[i].stats.R);
       content.push(req.body.awayPlayers.defensive[i].stats.ER);
       content.push(req.body.awayPlayers.defensive[i].stats.HBP);

        if((i + 1) != req.body.awayPlayers.defensive.length) {
          query += ', ';
        }
      }
      query += ') ';

      query += 'SELECT gameId FROM newGameId;';

      pool.query(query, content, function (err, respoce) {
        if(err) {
          console.log(err);
          res.status(500).end();
        }

        if(respoce.rows.length == 1) {
          var query1 = 'UPDATE the_bullpen.Game SET winningPitcher = $1, losingpitcher = $2, homeStarter = $3, awayStarter = $4' + ((save != -1)? (', savepitcher = $5'): ' ') + ' WHERE gameId = ' + respoce.rows[0].gameid + '::int; ';
          var content1 = [];
          content1.push(winning);
          content1.push(loosing);
          content1.push(homeStarter);
          content1.push(awayStarter);
          (save != -1)? content1.push(save): '';

          pool.query(query1, content1, function (err1, respoce1) {
            if(err1) {
              console.log(err1);
              return res.status(500).end();
            }

            res.send({gameId: respoce.rows[0].gameid});
            pool.query('SELECT the_bullpen.update_players();', null, function (err2, respoce2) {
              if(err2) {
                console.log(err2);
              }
            });

          });
        } else {
          return res.status(500).end();
        }
      });

    });
  } else {
    res.status(400).end();
  }
};

exports.addPlayer = function(req, res) {
  if(!req.session.user) {
    return res.status(404).end();
  } else if(!req.body.firstName || !req.body.lastName || !req.body.team || !req.body.num) {
    return res.status(400).end();
  } else if(typeof req.body.firstName !== "string" || !/^([a-zA-Z]+)$/.test(req.body.firstName) || typeof req.body.lastName !== "string" || !/^([a-zA-Z]+)$/.test(req.body.lastName) ||
            typeof req.body.team !== "string" || !/^([a-zA-Z]+)$/.test(req.body.team) || typeof req.body.num !== "number" || req.body.num < 0) {
    return res.status(400).end();
  }

  var query = 'SELECT 1 ' +
              'FROM the_bullpen.Account a LEFT OUTER JOIN the_bullpen.Team t ON a.accountId = t.managerId ' +
              'WHERE a.accountId = $1 AND (accountType = \'admin\' OR (accountType = \'manager\' AND t.teamName = $2 ) ); ';

  pool.query(query, [req.session.user, req.body.team], function(err, responce) {
    if(err) {
      console.log(err);
      res.status(500).end();
    }

    var query1 = 'INSERT INTO the_bullpen.Player(firstName, lastName, team, num) VALUES ($1::text, $2::text, $3::text, $4::integer) RETURNING playerId;';
    pool.query(query1, [req.body.firstName, req.body.lastName, req.body.team, req.body.num], function(err1, responce1) {
      if(err1) {
        console.log(err1);
        return req.status(500).end();
      }

      if(responce1.rows[0]) {
        res.send({playerNum: responce1.rows[0].playerid});

        pool.query('SELECT update_player($1::int);',[responce1.rows[0].playerid], function(err2, responce2) {
          if(err2)
            console.log(err2);
        });
      } else {
        return req.status(500).end();
      }
    });
  });
};

exports.addGamePage = function(req, res) {
  if(!req.session.user) {
    return exports.error(req, res);
  }

  var query = 'SELECT 1 ' +
              'FROM the_bullpen.Account a LEFT OUTER JOIN the_bullpen.Team t ON a.accountId = t.managerId ' +
              'WHERE a.accountId = $1 AND (accountType = \'admin\' OR (accountType = \'manager\' AND t.teamName IS NOT NULL ) ); ';

  pool.query(query, [req.session.user], function(err, responce) {
    if(err) {
      console.log(err);
      res.status(500).end();
    }

    if(responce.rows[0]) {
      res.sendFile(path.resolve('dinamicHtml/addGame.html'));
    } else {
      return res.status(404).end();
    }
  });
};

exports.getTeams = function(req, res) {
  query = 'SELECT teamName FROM the_bullpen.Team ORDER BY teamName';
  pool.query(query , null, function (err, queryRes) {
    if(err) {
      console.log(err);
      return res.status(500).end();
    }

    var responce = [];
    for(var i = 0; i < queryRes.rows.length; i++) {
      responce.push(queryRes.rows[i].teamname);
    }

    res.send(responce);

  });
};

exports.error = function(req, res) {
  res.status(404);
  res.sendFile(path.resolve('dinamicHtml/404.html'));
};
