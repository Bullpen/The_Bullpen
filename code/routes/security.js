var crypto = require('crypto');

exports.getSalt = function() {
     return crypto.randomBytes(8).toString('hex').slice(0, 16);
};

exports.getHash = function(password, salt) {
    var hash = crypto.createHmac('sha512', salt);
    return hash.update(password).digest('hex');
};

exports.verify = function(password, salt, hashed) {
    return hashed == exports.getHash(password, salt).substring(0, 128);
};