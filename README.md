# Stat Tracker

## What our application entails
Stat Tracker is a web application that allows users to keep track of baseball
stats for a league. The application keeps track of statistics for each game,
for each player, for each team, and the league over all.  All of this is stored,
and presented to the user through the web interface.

Through the same web interface, certain users are allowed to add new game that have
 been played but they do not need to calculate the stats, that is done form them by the application.

Some users are allowed to change user data, and promote or deny a promotion to other users.

## For developers

### How to use our code base
All of our code will be contained in the [code](./code) folder.

#### Getting started
The bellow command will start up the server and compile all of the necessary files, after you can go into your browser, and view the web page.

```
cd code
npm install
node server.js
```

#### Getting familiar with the code base

##### [assets](./code/assets)
All of the code in this folder will be directly available to out user upon request, this mean it shouldn't contain things we don't want the user getting their hands on.

##### [dinamicHtml](./code/dinamicHtml)
All files that shouldn't be readily available to the user, such as account files and player pages.  Any page that requires some back end validation goes here, and the routes files will intercept the request make sure the user should be making the specified request.

##### [Less](./code/Less)
This will contain all of the CSS we want to use in Less format.

All of the files that are in this folder will be available to the user in all areas of the website, no matter what page they are on.  This means please use Less's encapsulation if you aren't writing style that you will be using on a lot of pages.

It is probably a good idea to use a new file for every page you are designing, but that is not a strong requirement.

##### [routes](./code/routes)
The routes is the main back end portion of the application, it contains all of the responses to API calls that will be made, as well as all of the server calls that will be implemented.

##### [compile.js](./code/copile.js)
This file contains the logic for implementing all of the Less compilations that need to be done, it also takes a call back in it's main function, so that the rest of the server can be started up only after everything is complete, this means that when the server is started all of the Less is already complied and minified for you.

##### [server.js](./code/server.js)
This file contains all of the routing logic for where a particular request will go to, as well as all of the calls for different compilations that may be needed.
