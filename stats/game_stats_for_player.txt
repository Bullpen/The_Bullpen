Game Stats (per player):

Offensive: 

1B (Singles): count
2B (Doubles): count
3B (Triples): count
HR (Home Runs): count
H (Hits): 1B + 2B + 3B + HR
uBB (Unintentional Walks): count
iBB (Intentional Walks): count
BB (Walks): uBB + iBB
SF (Sacrifice Flies): count
HBP (Hit By Pitches): count
K (Strikeouts): count
PA (Plate Appearances): count
AB (At Bats): count
R (Runs Scored): count
RBI (Runs Batted In): count
SB (Stolen Bases): count
CS (Caught Stealing): count

Defensive / Pitching:

IP (Innings Pitched)
H (Hits)
K (Strikeouts): count
BB (Walks): count
HR (Homeruns): count
R (Runs): count
ER (Earned Runs): count
HBP (Hit Batsmen): count



