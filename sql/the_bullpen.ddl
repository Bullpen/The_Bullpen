DROP SCHEMA IF EXISTS the_bullpen cascade;
CREATE SCHEMA the_bullpen;
SET search_path TO the_bullpen, public;


CREATE DOMAIN accountType varchar(7)
       check (value in ('basic', 'manager', 'admin'));

CREATE TABLE Account (
	accountId SERIAL PRIMARY KEY,
	username varchar(64) UNIQUE NOT NULL,
	password varchar(128) NOT NULL,
  salt varchar(16) NOT NULL,
	email varchar(64),
	accountType accountType
);

CREATE TABLE Team (
	teamName varchar(25) PRIMARY KEY,
	homefield varchar(25),
	mascot varchar(25),
	logo varchar(25),
	managerId integer REFERENCES account(accountId)
	--team stats
);

CREATE TABLE Player (
	playerId SERIAL PRIMARY KEY,
	firstName  varchar(25) NOT NULL,
	lastName  varchar(25) NOT NULL,
	team varchar(25) NOT NULL REFERENCES Team(teamName),
  num integer NOT NULL CHECK (num >= 0),

	--life time stats
	B1 int,
	B2 int,
	B3 int,
	HR int,
	H int,
	uBB int,
	iBB int,
	BB int,
	SF int,
	HBP int,
	K int,
	G int,
	PA int,
	AB int,
	R int,
	RBI int,
	SB int,
	CS int,
	SBp float,
	BBp float,
	Kp float,
	bAVG float,
	BABIP float,
	OBP float,
	wOBA float,
	wRCplus float,

	--pitching stats
	--the word pitching is required here because otherwise we would have duplicate columns and sql doesn't like that
	Wpitcher int,
	Lpitcher int,
	SVpitcher int,
	Gpitcher int,
	GSpitcher int,
	IPpitcher int,
	IPfracPitcher int CHECK ((IPfracPitcher >= 0) AND (IPfracPitcher <= 2)) DEFAULT 0,
	Hpitcher int,
	Kpitcher int,
	BBpitcher int,
	HRpitcher int,
	Rpitcher int,
	ERpitcher int,
	HBPpitcher int,
	Kper9pitcher float,
	BBper9pitcher float,
	ERApitcher float,
	FIPpitcher float
);

CREATE TABLE Game (
	gameId SERIAL PRIMARY KEY,
	homeTeam varchar(25) NOT NULL REFERENCES Team(teamName),
	awayTeam varchar(25) NOT NULL REFERENCES Team(teamName),
	gameTime date NOT NULL,

	numInnings integer NOT NULL,
	winner varchar(25) NOT NULL REFERENCES Team(teamName), --the one with the larger score
	homeScore int NOT NULL, --sum of runs
	awayScore int NOT NULL, --sum of runs
	homeHits int NOT NULL, --sum of player hits
	awayHits int NOT NULL, ----sum of player hits
	winningPitcher integer,
	losingPitcher integer,
	savePitcher integer,
  homeStarter integer,
  awayStarter integer
);

CREATE TABLE player_game_offence (
	player integer NOT NULL REFERENCES Player(playerId),
	game integer NOT NULL REFERENCES game(gameId),
	PRIMARY KEY (player, game),

	--player stats in this game
	B1 int,
	B2 int,
	B3 int,
	HR int,
	H int, --computed
	uBB int,
	iBB int,
	BB int, --computed
	SF int,
	HBP int,
	K int,
	PA int,
	AB int,
	R int,
	RBI int,
	SB int,
	CS int
);

CREATE TABLE player_game_defence (
	player integer NOT NULL REFERENCES Player(playerId),
	game integer NOT NULL REFERENCES Game(gameId),
	PRIMARY KEY (player, game),

	--player stats in this game
	IP int,
	IPfrac int CHECK ((IPfrac >= 0) AND (IPfrac <= 2)) DEFAULT 0,
	H int,
	K int,
	BB int,
	HR int,
	R int,
	ER int,
	HBP int
);

CREATE TABLE upgradeRequests (
  requestId SERIAL PRIMARY KEY,
  accountId integer NOT NULL REFERENCES Account(accountId),
  requestTime timestamp NOT NULL DEFAULT NOW(),
  upgradeTo accountType NOT NULL,
  teamName varchar(25) REFERENCES Team(teamName),
  processor integer REFERENCES Account(accountId),
  processTime timestamp CHECK (processTime > requestTime),
  accepted boolean
);


CREATE FUNCTION team_manager_check() RETURNS trigger AS $team_manager_check$
BEGIN
    CASE
		WHEN ((NEW.managerId IS NOT NULL) AND ((SELECT accountType FROM the_bullpen.Account WHERE accountId = NEW.managerId) != 'manager'))
    THEN
        RAISE EXCEPTION 'Only Managers are allowed to be team managers';
		WHEN (SELECT 1 FROM the_bullpen.Team WHERE managerId = NEW.managerId) IS NOT NULL
		THEN
			RAISE EXCEPTION 'A user can manage at most one team';
		ELSE
			RETURN NEW;
    END CASE;
END;
$team_manager_check$ LANGUAGE plpgsql;

CREATE FUNCTION game_check() RETURNS trigger AS $game_check$
BEGIN
		CASE
		WHEN NEW.homeTeam = NEW.awayTeam
    THEN
        RAISE EXCEPTION 'A team can not play themselves';
		WHEN ((NEW.winner != NEW.homeTeam) AND (NEW.winner != NEW.awayTeam))
		THEN
			RAISE EXCEPTION 'The winning team must have played in the game';
		WHEN ((NEW.winningPitcher IS NOT NULL) AND ((SELECT 1 FROM the_bullpen.player_game_defence pgd WHERE NEW.gameId = pgd.game AND NEW.winningPitcher = pgd.player) IS NULL))
		THEN
			RAISE EXCEPTION 'The winning pitcher must have played in the game';
		WHEN ((NEW.losingPitcher IS NOT NULL) AND ((SELECT 1 FROM the_bullpen.player_game_defence pgd WHERE NEW.gameId = pgd.game AND NEW.losingPitcher = pgd.player) IS NULL)) --extra that they won
		THEN
			RAISE EXCEPTION 'The loosing pitcher must have played in the game';
		WHEN ((NEW.savePitcher IS NOT NULL) AND ((SELECT 1 FROM the_bullpen.player_game_defence pgd WHERE NEW.gameId = pgd.game AND NEW.savePitcher = pgd.player) IS NULL)) --extra checks
		THEN
			RAISE EXCEPTION 'The save pitcher must have played in the game';
    WHEN ((NEW.homeStarter IS NOT NULL) AND ((SELECT 1 FROM the_bullpen.player_game_defence pgd WHERE NEW.gameId = pgd.game AND NEW.homeStarter = pgd.player) IS NULL)) --extra checks
    THEN
      RAISE EXCEPTION 'The home starter must have played in the game';
    WHEN ((NEW.awayStarter IS NOT NULL) AND ((SELECT 1 FROM the_bullpen.player_game_defence pgd WHERE NEW.gameId = pgd.game AND NEW.awayStarter = pgd.player) IS NULL)) --extra checks
      THEN
        RAISE EXCEPTION 'The away starter must have played in the game';
		ELSE
			RETURN NEW;
    END CASE;
END;
$game_check$ LANGUAGE plpgsql;

CREATE FUNCTION player_game_check() RETURNS trigger AS $player_game_check$
BEGIN
    IF (SELECT 1
        FROM the_bullpen.GAME  g JOIN the_bullpen.PLAYER hp ON g.homeTeam = hp.team
  	    JOIN the_bullpen.Player ap ON g.awayTeam = ap.team
        WHERE ap.playerId = NEW.player OR hp.playerId = NEW.player
        LIMIT 1) IS NULL
    THEN
        RAISE EXCEPTION 'The player being entered did not play in the specified game';
    END IF;
    RETURN NEW;
END;
$player_game_check$ LANGUAGE plpgsql;

CREATE FUNCTION upgrade_requests_check() RETURNS trigger AS $upgrade_requests_check$
BEGIN
  CASE
    WHEN((NEW.processor IS NOT NULL) AND (SELECT 1 FROM the_bullpen.Account a WHERE a.accountId = NEW.processor AND a.accountType = 'admin') IS NULL)
    THEN
      RAISE EXCEPTION 'only admins can accept requests';
    WHEN (NEW.processor IS NULL) AND (SELECT 1 FROM the_bullpen.upgradeRequests ur WHERE ur.requestId != NEW.requestId AND ur.accountId = NEW.accountId AND ur.processor IS NULL) IS NOT NULL
    THEN
      RAISE EXCEPTION 'user already has an active request';
    ELSE
      RETURN NEW;
  END CASE;
END;
$upgrade_requests_check$ LANGUAGE plpgsql;


CREATE TRIGGER team_manager_check BEFORE INSERT OR UPDATE ON the_bullpen.Team
    FOR EACH ROW EXECUTE PROCEDURE team_manager_check();

CREATE TRIGGER game_check AFTER INSERT OR UPDATE ON the_bullpen.Game
    FOR EACH ROW EXECUTE PROCEDURE game_check();

CREATE TRIGGER player_game_offence_check AFTER INSERT OR UPDATE ON the_bullpen.player_game_offence
    FOR EACH ROW EXECUTE PROCEDURE player_game_check();

CREATE TRIGGER player_game_defence_check AFTER INSERT OR UPDATE ON the_bullpen.player_game_defence
    FOR EACH ROW EXECUTE PROCEDURE player_game_check();

CREATE TRIGGER upgrade_requests_check BEFORE INSERT OR UPDATE ON the_bullpen.upgradeRequests
    FOR EACH ROW EXECUTE PROCEDURE upgrade_requests_check();


CREATE OR REPLACE FUNCTION update_player(givenPlayerId integer) RETURNS void AS $$
DECLARE
	FipTotal RECORD;
  FipConstant float;

  WRCplusTotal RECORD;
  leaguewOBA float;
  leagueRoPA float;
BEGIN
  FOR FipTotal IN SELECT sum(ER) AS ER, ((sum(IP) + sum(IPfrac)/3)::float + (1.00/3.00)*((sum(IPfrac)%3)::float))::float AS IP, sum(HR) AS HR, sum(BB) AS BB, sum(HBP) AS HBP, sum(K) AS K FROM the_bullpen.player_game_defence
  LOOP
  FipConstant := CASE WHEN FipTotal.IP IS NOT NULL AND FipTotal.IP != 0 THEN (9.0*((FipTotal.ER::float)/(FipTotal.IP::float)) - (((13 * FipTotal.HR)::float + (3 * (FipTotal.BB  + FipTotal.HBP))::float - (2 * FipTotal.K))::float/(FipTotal.IP::float))::float) ELSE NULL END;

  FOR WRCplusTotal IN SELECT sum(uBB) AS uBB, sum(HBP) AS HBP, sum(B1) AS B1, sum(B2) AS B2, sum(B3) AS B3, sum(HR) AS HR, sum(AB) AS AB, sum(iBB) + sum(uBB) AS BB, sum(iBB) AS iBB, sum(SF) AS SF, sum(HBP) AS HBP, sum(R) AS R, sum(PA) AS PA FROM the_bullpen.player_game_offence
  LOOP

    leaguewOBA := CASE WHEN (WRCplusTotal.AB + WRCplusTotal.BB - WRCplusTotal.iBB + WRCplusTotal.SF + WRCplusTotal.HBP) != 0 AND (WRCplusTotal.AB + WRCplusTotal.BB - WRCplusTotal.iBB + WRCplusTotal.SF + WRCplusTotal.HBP) IS NOT NULL THEN
                    ((.69*WRCplusTotal.uBB::float + .72*WRCplusTotal.HBP::float + .88*WRCplusTotal.B1::float + 1.24*WRCplusTotal.B2::float + 1.57*WRCplusTotal.B3::float + 2.02*WRCplusTotal.HR::float)/
                    ((WRCplusTotal.AB + WRCplusTotal.BB - WRCplusTotal.iBB + WRCplusTotal.SF + WRCplusTotal.HBP)::float))::float
                  ELSE NULL END;

    leagueRoPA := CASE WHEN WRCplusTotal.PA != 0 AND WRCplusTotal.PA IS NOT NULL THEN
                      ((WRCplusTotal.R::float)/(WRCplusTotal.PA::float))::float
                  ELSE NULL END;

    UPDATE the_bullpen.Player SET
      B1 = statsPGO.B1,
      B2 = statsPGO.B2,
      B3 = statsPGO.B3,
      HR = statsPGO.HR,
      H = statsPGO.H,
      uBB = statsPGO.uBB,
      iBB = statsPGO.iBB,
      BB = statsPGO.BB,
      SF = statsPGO.SF,
      HBP = statsPGO.HBP,
      K = statsPGO.K,
      G = statsPGO.G,
      PA = statsPGO.PA,
      AB = statsPGO.AB,
      R = statsPGO.R,
      RBI = statsPGO.RBI,
      SB = statsPGO.SB,
      CS = statsPGO.CS,
      SBp = CASE WHEN (statsPGO.SB + statsPGO.CS) != 0 AND (statsPGO.SB + statsPGO.CS) IS NOT NULL THEN ((statsPGO.SB::float)/((statsPGO.SB + statsPGO.CS)::float))::float ELSE NULL END,
      BBp = CASE WHEN (statsPGO.PA) != 0 AND (statsPGO.PA) IS NOT NULL THEN ((statsPGO.BB::float)/(statsPGO.PA::float))::float ELSE NULL END,
      Kp = CASE WHEN (statsPGO.PA) != 0 AND (statsPGO.PA) IS NOT NULL THEN ((statsPGO.K::float)/(statsPGO.PA::float))::float ELSE NULL END,
      bAVG = CASE WHEN (statsPGO.AB) != 0 AND (statsPGO.AB) IS NOT NULL THEN ((statsPGO.H::float)/(statsPGO.AB::float))::float ELSE NULL END,
      BABIP = CASE WHEN (statsPGO.AB - statsPGO.K - statsPGO.HR + statsPGO.SF) != 0 AND (statsPGO.AB - statsPGO.K - statsPGO.HR + statsPGO.SF) IS NOT NULL THEN (((statsPGO.H - statsPGO.HR)::float)/((statsPGO.AB - statsPGO.K - statsPGO.HR + statsPGO.SF)::float))::float ELSE NULL END,
      OBP = CASE WHEN (statsPGO.AB + statsPGO.BB + statsPGO.HBP + statsPGO.SF) != 0 AND (statsPGO.AB + statsPGO.BB + statsPGO.HBP + statsPGO.SF) IS NOT NULL THEN (((statsPGO.H + statsPGO.BB + statsPGO.HBP)::float)/((statsPGO.AB + statsPGO.BB + statsPGO.HBP + statsPGO.SF)::float))::float ELSE NULL END,
      wOBA = CASE WHEN (statsPGO.AB + statsPGO.BB - statsPGO.iBB + statsPGO.SF + statsPGO.HBP) != 0 AND (statsPGO.AB + statsPGO.BB - statsPGO.iBB + statsPGO.SF + statsPGO.HBP) IS NOT NULL THEN ((.69*statsPGO.uBB::float + .72*statsPGO.HBP::float + .88*statsPGO.B1::float + 1.24*statsPGO.B2::float + 1.57*statsPGO.B3::float + 2.02*statsPGO.HR::float)/((statsPGO.AB + statsPGO.BB - statsPGO.iBB + statsPGO.SF + statsPGO.HBP)::float))::float ELSE NULL END,
      wRCplus = CASE WHEN statsPGO.PA != 0 AND statsPGO.PA IS NOT NULL AND (statsPGO.AB + statsPGO.BB - statsPGO.iBB + statsPGO.SF + statsPGO.HBP) != 0 AND (statsPGO.AB + statsPGO.BB - statsPGO.iBB + statsPGO.SF + statsPGO.HBP) IS NOT NULL AND leaguewOBA IS NOT NULL THEN
                    (100*(1.00 + (((((.69*statsPGO.uBB::float + .72*statsPGO.HBP::float + .88*statsPGO.B1::float + 1.24*statsPGO.B2::float + 1.57*statsPGO.B3::float + 2.02*statsPGO.HR::float)/((statsPGO.AB + statsPGO.BB - statsPGO.iBB + statsPGO.SF + statsPGO.HBP)::float))::float) - leaguewOBA::float)/(1.21 * leagueRoPA::float)::float)::float)::float)::integer
                ELSE NULL END
      FROM (SELECT SUM(pgo.B1) AS B1, SUM(pgo.B2) AS B2, SUM(pgo.B3) AS B3, SUM(pgo.HR) AS HR, SUM(pgo.H) AS H, SUM(pgo.uBB) AS uBB, SUM(pgo.iBB) AS iBB, SUM(pgo.BB) AS BB,
                   SUM(pgo.SF) AS SF, SUM(pgo.HBP) AS HBP, SUM(pgo.K) AS K, count(*) AS G, SUM(pgo.PA) AS PA, SUM(pgo.AB) AS AB, SUM(pgo.R) AS R, SUM(pgo.RBI) AS RBI, SUM(pgo.SB) AS SB, SUM(pgo.CS) AS CS
            FROM the_bullpen.Player p JOIN the_bullpen.player_game_offence pgo ON p.playerId = pgo.player
            WHERE p.playerId = givenPlayerId
            GROUP BY p.playerId) AS statsPGO
      WHERE playerId = givenPlayerId;

    UPDATE the_bullpen.Player SET
      Gpitcher = statsPGD.games,
      IPpitcher = statsPGD.IP,
      IPfracPitcher = statsPGD.IPfrac,
      Hpitcher = statsPGD.H,
      Kpitcher = statsPGD.K,
      BBpitcher = statsPGD.BB,
      HRpitcher = statsPGD.HR,
      Rpitcher = statsPGD.R,
      ERpitcher = statsPGD.ER,
      HBPpitcher = statsPGD.HBP,
      Kper9pitcher = CASE WHEN (statsPGD.IP::float + (statsPGD.IPfrac::float*(1.00/3.00))::float) != 0 AND (statsPGD.IP::float + (statsPGD.IPfrac::float*(1.00/3.00))::float) IS NOT NULL THEN 9.0*(statsPGD.K::float/(statsPGD.IP::float + (statsPGD.IPfrac::float*(1.00/3.00))::float)::float)::float ELSE NULL END,
      BBper9pitcher = CASE WHEN (statsPGD.IP::float + (statsPGD.IPfrac::float*(1.00/3.00))::float) != 0 AND (statsPGD.IP::float + (statsPGD.IPfrac::float*(1.00/3.00))::float) IS NOT NULL THEN 9.0*(statsPGD.BB::float/(statsPGD.IP::float + (statsPGD.IPfrac::float*(1.00/3.00))::float)::float)::float ELSE NULL END,
      ERApitcher = CASE WHEN (statsPGD.IP::float + (statsPGD.IPfrac::float*(1.00/3.00))::float) != 0 AND (statsPGD.IP::float + (statsPGD.IPfrac::float*(1.00/3.00))::float) IS NOT NULL THEN 9.0*(statsPGD.ER::float/(statsPGD.IP::float + (statsPGD.IPfrac::float*(1.00/3.00))::float)::float)::float ELSE NULL END,
      FIPpitcher = CASE WHEN (statsPGD.IP::float + (statsPGD.IPfrac::float*(1.00/3.00))::float) != 0 AND (statsPGD.IP::float + (statsPGD.IPfrac::float*(1.00/3.00))::float) IS NOT NULL THEN (((13.0*statsPGD.HR + 3*(statsPGD.BB + statsPGD.HBP) - 2*statsPGD.K)::float)/((statsPGD.IP::float + (statsPGD.IPfrac::float*(1.00/3.00))::float)::float)) + FipConstant::float ELSE NULL END
      FROM (SELECT count(*) AS games, (SUM(pgd.IP) + (SUM(pgd.IPfrac)/3)::int) AS IP, (SUM(pgd.IPfrac)%3)::int AS IPfrac, SUM(pgd.H) AS H, SUM(pgd.K) AS K,
                   SUM(pgd.BB) AS BB, SUM(pgd.HR) AS HR, SUM(pgd.R) AS R, SUM(pgd.ER) AS ER, SUM(pgd.HBP) AS HBP
            FROM the_bullpen.Player p JOIN the_bullpen.player_game_defence pgd ON p.playerId = pgd.player
            WHERE p.playerId = givenPlayerId
            GROUP BY p.playerId) AS statsPGD
      WHERE playerId = givenPlayerId;

    UPDATE the_bullpen.Player SET
    Wpitcher = stats.losses,
    Lpitcher = stats.wins,
    SVpitcher = stats.saves,
    GSpitcher = stats.starts
    FROM (SELECT count(CASE WHEN g.winningPitcher = p.playerId THEN 1 END) AS wins, count(CASE WHEN g.losingPitcher = p.playerId THEN 1 END) AS losses, count(CASE WHEN g.savePitcher = p.playerId THEN 1 END) AS saves,
                 count(CASE WHEN g.homeStarter = p.playerId OR g.awayStarter = p.playerId THEN 1 END) AS starts
          FROM the_bullpen.Player p JOIN the_bullpen.Game g ON p.playerId = g.winningPitcher OR p.playerId = g.losingPitcher OR p.playerId = g.savePitcher OR p.playerId = g.homeStarter OR p.playerId = g.awayStarter
          WHERE p.playerId = givenPlayerId
          GROUP BY p.playerId) stats
    WHERE playerId = givenPlayerId;

  END LOOP;
  END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_players() RETURNS void AS $$
DECLARE
	p RECORD;
BEGIN
	FOR p IN SELECT playerId FROM the_bullpen.Player
	LOOP
    RAISE NOTICE 'trying to update %', p.playerId;
		EXECUTE 'SELECT the_bullpen.update_player(' || p.playerId  || ');';
	END LOOP;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION request_responce(processor1 integer, status boolean, request integer) RETURNS void AS $$
BEGIN
  CASE
    WHEN (SELECT upgradeTo FROM the_bullpen.upgradeRequests WHERE requestId = request) = 'admin'
    THEN
      UPDATE the_bullpen.upgradeRequests ur SET processor = processor1, processTime = NOW(), accepted = status WHERE ur.requestId = request;
      IF status = true
      THEN
        UPDATE the_bullpen.Account SET accountType = 'admin' WHERE accountId = (SELECT accountId FROM the_bullpen.upgradeRequests ur WHERE ur.requestId = request);
      END IF;
    WHEN (SELECT upgradeTo FROM the_bullpen.upgradeRequests WHERE requestId = request) = 'manager'
    THEN
      UPDATE the_bullpen.upgradeRequests ur SET processor = processor1, processTime = NOW(), accepted = status WHERE ur.requestId = request;
      IF status = true
      THEN
        UPDATE the_bullpen.Account SET accountType = 'manager' WHERE accountId = (SELECT accountId FROM the_bullpen.upgradeRequests ur WHERE ur.requestId = request);
        UPDATE the_bullpen.Team SET managerId = (SELECT accountId FROM the_bullpen.upgradeRequests ur WHERE ur.requestId = request)
          WHERE teamName = (SELECT teamName FROM the_bullpen.upgradeRequests ur WHERE ur.requestId = request);
      END IF;
  END CASE;
END;
$$ LANGUAGE plpgsql;
