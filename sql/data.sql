SET search_path TO the_bullpen, public;
TRUNCATE TABLE Account CASCADE;
TRUNCATE TABLE Team CASCADE;
TRUNCATE TABLE Player CASCADE;
TRUNCATE TABLE Game CASCADE;
TRUNCATE TABLE player_game_offence CASCADE;
TRUNCATE TABLE player_game_defence CASCADE;

-- password is 'password'
INSERT INTO Account(username, password, salt, email, accountType) VALUES ('u1', 'ba382feb653cb06746e65d13209db807bfbe33cbcb047432c1130b41c956a3833d6c927e41c44148a82c27b1fbc32a35cf83ec347df1cfa8bb59046a578695e0', '', 'eamil1@mail.com', 'admin');
INSERT INTO Account(username, password, salt, email, accountType) VALUES ('u2', 'ba382feb653cb06746e65d13209db807bfbe33cbcb047432c1130b41c956a3833d6c927e41c44148a82c27b1fbc32a35cf83ec347df1cfa8bb59046a578695e0', '', 'eamil2@mail.com', 'manager');
INSERT INTO Account(username, password, salt, email, accountType) VALUES ('u3', 'ba382feb653cb06746e65d13209db807bfbe33cbcb047432c1130b41c956a3833d6c927e41c44148a82c27b1fbc32a35cf83ec347df1cfa8bb59046a578695e0', '', 'eamil3@mail.com', 'basic');

--add team managers
INSERT INTO Team(teamName, managerId) VALUES ('Toronto', 2);
INSERT INTO Team(teamName) VALUES ('Philadelphia');
INSERT INTO Team(teamName) VALUES ('Mumbai');
INSERT INTO Team(teamName) VALUES ('Kodak');

-- TORONTO
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Vugar', 'Mammadli', 'Toronto', 13); -- 1
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Mike', 'Shulman', 'Toronto', 45); -- 2
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Ben', 'Grass', 'Toronto', 17); -- 3
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Liam', 'Kelly', 'Toronto', 25); -- 4
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Brayden', 'Kell', 'Toronto', 7); -- 5
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Sam', 'Jenison', 'Toronto', 21); -- 6
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Nigel', 'Fong', 'Toronto', 9); -- 7
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Blake', 'Donovan', 'Toronto', 8); --8
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Jai', 'Nimgaonkar', 'Toronto', 5); --9
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Caelan', 'Wilton', 'Toronto', 16); --10

-- PHILADELPHIA
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Robert', 'Sedgewick', 'Philadelphia', 1); -- 11
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Daniel', 'Bernstein', 'Philadelphia', 2); -- 12
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Donald', 'Knuth', 'Philadelphia', 4); -- 13
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Brian', 'Kernighan', 'Philadelphia', 8); -- 14
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Edsger', 'Dijkstra', 'Philadelphia', 16); -- 15
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Alan', 'Turing', 'Philadelphia', 32); -- 16
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Tim', 'BernersLee', 'Philadelphia', 64); -- 17
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Bjarne', 'Stroustrup', 'Philadelphia', 28); -- 18
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Linus', 'Torvalds', 'Philadelphia', 56); -- 19
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Ken', 'Thompson', 'Philadelphia', 12); -- 20

-- MUMBAI
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Ravi', 'Jay', 'Mumbai', 10); -- 21
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Kaushal', 'Black', 'Mumbai', 18); -- 22
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Jashybaba', 'Porterfield', 'Mumbai', 1); -- 23
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Maximus', 'Lilly', 'Mumbai', 99); -- 24
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Shay', 'Birathian', 'Mumbai', 11); -- 25
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Maura', 'Turner', 'Mumbai', 44); -- 26
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Meircat', 'Maya', 'Mumbai', 0); -- 27
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Raena', 'Porter', 'Mumbai', 14); -- 28
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Joseph', 'Gross', 'Mumbai', 12); -- 29
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Anand', 'Deven', 'Mumbai', 2); -- 30

-- KODAK
INSERT INTO Player(firstName, lastName, team, num) VALUES ('David', 'G', 'Kodak', 20); -- 31
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Dave', 'Wet', 'Kodak', 13); -- 32
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Johnny', 'Bravo', 'Kodak', 7); -- 33
INSERT INTO Player(firstName, lastName, team, num) VALUES ('SpearIt', 'Maldonado', 'Kodak', 12); -- 34
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Keith', 'Port', 'Kodak', 1); -- 35
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Jaimey', 'Karhoff', 'Kodak', 10); -- 36
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Manan', 'Gupta', 'Kodak', 23); -- 37
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Minesh', 'Rad', 'Kodak', 31); -- 38
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Yin', 'Song', 'Kodak', 44); -- 39
INSERT INTO Player(firstName, lastName, team, num) VALUES ('Dada', 'Patel', 'Kodak', 22); -- 40



-- TOR @ PHL

INSERT INTO Game(homeTeam, awayTeam, gameTime, numInnings, winner, homeScore, awayScore, homeHits, awayHits)
		 VALUES ('Toronto', 'Philadelphia', now(), '9', 'Toronto', '3', '2', '9', '6'); -- 1
-- tor off
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '4', '4', '0', '0', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('8', '1', '1', '1', '0', '0', '2', '0', '0', '0', '0', '0', '1', '4', '4', '2', '0', '1', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('3', '1', '2', '0', '0', '0', '2', '1', '0', '1', '0', '0', '1', '4', '3', '0', '0', '0', '1');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('2', '1', '2', '1', '0', '0', '3', '0', '0', '0', '0', '0', '0', '4', '4', '0', '2', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('4', '1', '1', '0', '0', '0', '1', '2', '0', '2', '0', '0', '0', '4', '2', '0', '0', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('5', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '4', '4', '0', '0', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('6', '1', '0', '0', '0', '0', '0', '1', '0', '1', '0', '0', '2', '3', '2', '0', '0', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('7', '1', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '3', '3', '1', '1', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('9', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '3', '3', '0', '0', '0', '0');
-- phl off
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('11', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '3', '4', '4', '0', '0', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('12', '1', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '3', '4', '4', '1', '1', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('13', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '3', '4', '4', '0', '0', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('14', '1', '3', '0', '0', '0', '3', '0', '0', '0', '0', '0', '0', '4', '4', '0', '0', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('15', '1', '0', '1', '0', '0', '1', '0', '0', '0', '0', '0', '2', '4', '4', '0', '0', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('16', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '3', '4', '4', '0', '0', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('17', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '3', '3', '3', '0', '0', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('18', '1', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '1', '3', '3', '1', '1', '0', '0');
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES ('19', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '3', '3', '0', '0', '0', '0');

-- phs def
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES ('10', '1', '9', '0', '6', '20', '0', '2', '2', '2', '0');


-- phl def
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES ('20', '1', '7', '0', '7', '3', '3', '1', '3', '3', '0');
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES ('15', '1', '0', '2', '2', '1', '0', '0', '0', '0', '0');
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES ('18', '1', '0', '1', '0', '0', '1', '0', '0', '0', '0');

--add starters
UPDATE Game SET awayStarter = 20, homeStarter = 10, winningPitcher = 10, losingPitcher = 20 WHERE gameId = 1;




-- KDK vs MUM

INSERT INTO Game(homeTeam, awayTeam, gameTime, numInnings, winner, homeScore, awayScore, homeHits, awayHits)
		 VALUES ('Kodak', 'Mumbai', now(), '9', 'Mumbai', '3', '7', '7', '9'); -- 2
-- KDK off
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (31,2,0,0,0,0,0,2,0,2,0,1,1,5,2,1,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (32,2,0,1,0,0,1,0,0,0,0,0,1,5,5,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (33,2,0,0,0,0,0,0,0,0,0,0,0,4,4,0,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (34,2,0,1,0,1,2,0,0,0,0,0,1,4,4,1,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (35,2,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (36,2,3,0,0,0,3,0,0,0,0,0,1,4,4,1,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (37,2,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (38,2,1,0,0,0,1,0,0,0,0,0,0,4,4,0,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (39,2,0,0,0,0,0,1,0,1,0,0,0,4,3,0,0,0,0);
-- mum off
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (21,2,1,0,0,0,1,0,0,0,0,1,1,5,4,0,0,0,1);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (22,2,1,0,0,0,1,2,0,2,0,1,1,5,2,1,2,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (23,2,2,0,0,0,2,0,0,0,0,0,1,5,5,1,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (24,2,0,0,0,1,1,1,0,2,0,0,0,5,4,2,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (25,2,0,0,0,0,0,1,0,2,1,1,1,5,2,0,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (26,2,0,0,0,1,1,1,0,2,0,0,1,5,4,1,3,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (27,2,1,0,0,0,1,0,0,0,0,0,0,5,5,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (28,2,1,0,0,0,1,1,0,1,0,0,2,5,3,1,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (29,2,1,0,0,0,1,0,0,0,0,0,0,4,4,1,0,0,0);

-- KDK def
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (40,2,5,2,6,7,4,1,6,6,2);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (38,2,3,1,3,0,2,1,1,1,1);

-- mum def
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (30,2,6,0,6,1,3,1,3,3,0);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (22,2,2,0,1,2,0,0,0,0,1);

--add starters
UPDATE Game SET homeStarter = 40, awayStarter = 30, winningPitcher = 30, losingPitcher = 40 WHERE gameId = 2;


-- TOR vs KDK

INSERT INTO Game(homeTeam, awayTeam, gameTime, numInnings, winner, homeScore, awayScore, homeHits, awayHits)
		 VALUES ('Toronto', 'Kodak', now(), '9', 'Toronto', '17', '1', '18', '5'); -- 3
-- tor off
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (1,3,1,0,0,0,1,2,0,2,0,0,0,6,3,2,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (8,3,3,1,0,0,4,1,1,2,0,0,0,6,4,4,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (3,3,0,2,0,0,2,0,0,0,1,0,1,6,5,2,3,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (2,3,1,2,0,0,3,1,0,1,0,0,1,6,5,2,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (4,3,2,0,0,1,3,1,1,2,0,0,0,6,4,3,5,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (5,3,2,0,0,1,3,0,0,0,1,0,0,6,5,1,4,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (6,3,0,0,0,1,1,0,0,0,0,0,2,6,6,1,2,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (7,3,0,0,0,0,0,2,0,2,0,0,0,6,4,1,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (9,3,1,0,0,0,1,0,0,1,0,0,1,5,4,1,0,0,1);
-- KDK off
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (31,3,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (32,3,0,1,0,0,1,0,0,0,0,0,1,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (33,3,0,0,0,0,0,0,0,0,0,0,3,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (34,3,0,0,0,0,0,0,0,0,0,0,2,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (35,3,1,1,0,0,2,0,0,0,0,0,1,4,4,1,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (36,3,1,0,0,0,1,0,0,0,0,0,1,3,3,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (37,3,1,0,0,0,1,0,0,0,0,0,1,3,3,0,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (38,3,0,0,0,0,0,0,0,0,0,0,2,3,3,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (39,3,0,0,0,0,0,0,0,0,0,0,2,3,3,0,0,0,0);

-- tor def
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (3,3,7,0,5,11,0,0,1,1,0);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (4,3,1,0,0,1,0,0,0,0,0);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (9,3,1,0,0,2,0,0,0,0,0);

-- KDK def
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (32,3,3,1,7,4,4,1,5,5,0);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (38,3,2,0,4,1,6,0,7,7,0);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (39,3,2,2,7,1,0,1,5,5,0);

--add starters
UPDATE Game SET homeStarter = 3, awayStarter = 32, winningPitcher = 3, losingPitcher = 32 WHERE gameId = 3;



-- Mumbai vs Philly

INSERT INTO Game(homeTeam, awayTeam, gameTime, numInnings, winner, homeScore, awayScore, homeHits, awayHits)
		 VALUES ('Mumbai', 'Philadelphia', now(), '9', 'Mumbai', '2', '1', '4', '7'); -- 4
-- MUM off
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (21,4,0,0,0,0,0,1,0,1,0,0,2,4,3,1,0,1,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (22,4,0,0,0,0,0,0,0,0,0,0,2,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (23,4,1,0,1,0,2,0,0,0,0,0,1,4,4,1,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (24,4,1,0,0,0,1,0,0,0,0,0,1,3,3,0,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (25,4,0,0,0,0,0,0,0,0,0,1,1,3,2,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (26,4,0,0,0,0,0,0,0,0,0,0,9,3,3,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (27,4,0,0,0,0,0,0,0,0,0,0,1,3,2,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (28,4,1,0,0,0,1,0,0,0,0,0,0,3,3,0,0,0,1);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (29,4,0,0,0,0,0,0,0,0,0,0,3,3,3,0,0,0,0);
-- PHL off
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (11,4,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (12,4,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (13,4,2,0,0,0,2,0,0,0,0,0,0,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (14,4,1,0,0,0,1,0,0,0,0,0,0,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (15,4,0,0,0,0,0,0,0,0,0,0,2,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (16,4,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (17,4,2,0,0,0,2,0,0,0,0,0,1,3,3,1,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (18,4,1,0,0,0,1,0,0,0,0,0,0,3,3,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (19,4,1,0,0,0,1,0,0,0,0,0,0,3,3,0,1,0,0);

-- MUM def
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (21,4,9,0,7,6,0,0,1,1,0);

-- PHL def
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (11,4,4,0,1,5,0,0,0,0,0);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (15,4,3,0,3,3,1,0,2,2,0);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (18,4,1,0,0,3,0,0,0,0,0);

--add starters
UPDATE Game SET homeStarter = 21, awayStarter = 11, winningPitcher = 21, losingPitcher = 15 WHERE gameId = 4;



-- TOR vs MUM

INSERT INTO Game(homeTeam, awayTeam, gameTime, numInnings, winner, homeScore, awayScore, homeHits, awayHits)
		 VALUES ('Toronto', 'Mumbai', now(), '9', 'Toronto', '6', '2', '6', '4'); -- 5
-- TOR off
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (1,5,0,0,0,0,0,1,0,1,0,0,1,4,2,1,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (8,5,1,0,0,0,1,1,0,1,0,0,0,4,3,0,2,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (3,5,1,0,0,0,1,0,0,0,0,0,1,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (2,5,0,0,0,0,0,0,0,0,0,0,1,4,4,0,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (4,5,0,0,0,0,0,1,0,1,0,0,1,4,3,1,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (5,5,2,0,0,0,2,1,0,1,0,0,0,4,3,1,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (6,5,1,0,0,0,1,0,0,0,0,1,0,3,3,1,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (7,5,0,0,0,0,0,1,0,1,0,0,0,3,2,1,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (9,5,1,0,0,0,1,0,0,0,0,0,1,3,3,1,2,0,0);
-- MUM off
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (21,5,0,0,0,0,0,0,0,0,0,0,0,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (22,5,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (23,5,0,0,0,1,1,0,0,0,0,0,1,4,4,1,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (24,5,0,0,0,1,1,0,0,0,0,0,0,4,4,1,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (25,5,0,0,0,0,0,0,0,0,0,0,1,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (26,5,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (27,5,1,0,0,0,1,0,0,0,0,0,1,3,3,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (28,5,0,0,0,0,0,1,0,1,0,0,0,3,2,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (29,5,1,0,0,0,1,0,0,0,0,0,0,3,3,0,0,0,0);

-- TOR def
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (10,5,8,0,3,3,1,1,1,1,0);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (4,5,1,0,1,1,0,1,1,1,0);

-- MUM def
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (30,5,6,0,4,5,3,0,4,4,1);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (22,5,1,0,2,0,1,0,2,2,0);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (29,5,1,0,0,0,1,0,0,0,0);

--add starters
UPDATE Game SET homeStarter = 10, awayStarter = 30, winningPitcher = 10, losingPitcher = 30, savePitcher = 4 WHERE gameId = 5;


-- PHL vs KDK

INSERT INTO Game(homeTeam, awayTeam, gameTime, numInnings, winner, homeScore, awayScore, homeHits, awayHits)
		 VALUES ('Philadelphia', 'Kodak', now(), '9', 'Kodak', '1', '2', '4', '9'); -- 6
-- PHL off
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (11,6,0,0,0,0,0,0,0,0,0,0,2,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (12,6,0,1,0,0,1,0,0,0,0,0,3,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (13,6,0,0,0,0,0,1,0,1,0,0,3,4,3,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (14,6,2,0,0,0,2,0,0,0,0,0,2,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (15,6,0,0,0,1,1,0,0,0,0,0,2,4,4,1,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (16,6,0,0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (17,6,0,0,0,0,0,0,0,0,0,0,1,3,3,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (18,6,0,0,0,0,0,0,0,0,0,0,1,3,2,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (19,6,0,0,0,0,0,1,0,1,0,0,0,3,1,0,0,0,0);
-- KDK off
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (31,6,0,1,1,0,2,0,0,0,0,0,0,5,5,1,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (32,6,0,1,0,0,1,0,0,0,0,1,1,5,4,0,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (33,6,0,0,0,0,0,0,0,0,0,0,0,5,5,0,1,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (34,6,2,0,0,0,2,0,0,0,0,0,0,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (35,6,1,0,0,0,1,0,0,0,0,0,0,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (36,6,1,0,0,0,1,0,0,0,0,0,0,4,4,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (37,6,0,0,0,0,0,1,0,1,0,0,1,4,3,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (38,6,1,0,0,0,1,0,0,0,0,1,0,4,3,0,0,0,0);
INSERT INTO player_game_offence(player, game, B1, B2, B3, HR, H, uBB, iBB, BB, SF, HBP, K, PA, AB, R, RBI, SB, CS)
						VALUES (39,6,1,0,0,0,1,0,0,0,0,0,2,4,4,1,0,1,0);

-- PHL def
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (20,6,6,0,7,3,1,0,2,2,2);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (15,6,1,0,1,0,0,0,0,0,0);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (18,6,2,0,1,1,0,0,0,0,0);

-- KDK def /// changed IPfrac to 2
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (40,6,7,0,3,9,1,1,1,1,0);
INSERT INTO player_game_defence(player, game, IP, IPfrac, H, K, BB, HR, R, ER, HBP)
						VALUES (38,6,2,0,1,5,1,0,0,0,0);

--add starters
UPDATE Game SET homeStarter = 20, awayStarter = 40, winningPitcher = 40, losingPitcher = 20, savePitcher = 38 WHERE gameId = 6;




SELECT update_players();
